<?php         defined('C5_EXECUTE') or die(_("Access Denied."));
global $c;
if ($c->isEditMode() == TRUE) {
if ($showTitle == 0) {
      $thumbBottommargin = 6;
    } else {
      $thumbBottommargin = intval($maxThumbHeight * 0.075 * 2) + 25;
    }

?>
<style>
#touchgallery-thumbs<?php      echo $bID ?> a{
  width:<?php      echo $maxThumbWidth ?>px;
  height:<?php      echo $maxThumbHeight ?>px;
  margin: 6px 6px <?php    echo $thumbBottommargin ?>px;
}
#touchgallery-thumbs<?php      echo $bID ?> a:after{
    bottom: -<?php      echo intval(($maxThumbHeight * 1.075) + 7) ?>px; 
    max-width: <?php      echo intval($maxThumbWidth - 10) ?>px;
    <?php      if ($showTitle == 0) {echo 'display:none;';} ?>
}
</style>
<?php    } ?>
<div class="touchgallery-onlyfirstthumb" id="touchgallery-thumbs<?php     echo $bID ?>">
<?php $is_first = true; ?>
	<?php         foreach ($images as $img): ?>
		<?php if($is_first) { ?>
			<a href="<?php     echo $img->large->src ?>" style="margin:0;"><img src="/packages/theme_slate/themes/slate/images/sonnenhaus_gallery.jpg" title="Sonnenhaus Galerie" /></a>
		<?php 	$is_first = false; ?>
		<?php } else { ?>
			<a href="<?php     echo $img->large->src ?>" style="display:none;" title="<?php     echo $img->title ?>"<?php     if ($img->description && $showDesc) { echo ' data-tg-desc="' . $img->description . '"';} ?>></a>			
		<?php } ?>		
	<?php         endforeach; ?>

</div>