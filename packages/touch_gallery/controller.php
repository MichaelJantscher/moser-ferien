<?php         defined('C5_EXECUTE') or die(_("Access Denied."));

class TouchGalleryPackage extends Package {

	protected $pkgHandle = 'touch_gallery';
	protected $appVersionRequired = '5.4.1';
	protected $pkgVersion = '1.4.3';
	
	public function getPackageName() {
		return t('Touch Gallery');
	}
	
	public function getPackageDescription() {
		return t('A light weight, fully responsive, touch enabled image gallery with intelligent preloading.');
	}
	
	public function install() {
		$pkg = parent::install();
		BlockType::installBlockTypeFromPackage($this->pkgHandle, $pkg);
		$this->installPageLinkAttribute($pkg);
	}
	
	public function upgrade() {
		$pkg = Package::getByHandle($this->pkgHandle);
		$this->installPageLinkAttribute($pkg);
		parent::upgrade();
		// alter the table to add the 2 fileds, one is INT(10) and the other TINYINT(4)
		$db = Loader::db();
		// $db->Execute("ALTER TABLE 'btTouchGallery' ADD column 'hideControls' tinyint(4) NULL DEFAULT 1");
		// $db->Execute("ALTER TABLE 'btTouchGallery' ADD column 'hideDelay' INT(10) DEFAULT 3;");
		// $db->Execute('alter table Pages add column cIsSystemPage tinyint(1) not null default 0');
		// $db->Execute("ALTER TABLE `CoreCommerceProducts` CHANGE `prQuantity` `prQuantity` INT(10)  NOT NULL  DEFAULT '0'");
	}
	
	private function installPageLinkAttribute(&$pkg) {
		$at = AttributeType::getByHandle('page_selector');
		if ($at && intval($at->getAttributeTypeID())) {
			//Associate with "file" category (if not done alrady)
			Loader::model('attribute/categories/collection');
			$akc = AttributeKeyCategory::getByHandle('file');
			$sql = 'SELECT COUNT(*) FROM AttributeTypeCategories WHERE atID = ? AND akCategoryID = ?';
			$vals = array($at->getAttributeTypeID(), $akc->akCategoryID);
			$existsInCategory = Loader::db()->GetOne($sql, $vals);
			if (!$existsInCategory) {
				$akc->associateAttributeKeyType($at);
			}
			
			//Install the link-to-page attribute (if not done already)
			Loader::model('file_attributes');
			$akHandle = 'gallery_link_to_cid';
			$akGalleryLinkToCID = FileAttributeKey::getByHandle($akHandle);
			if (!is_object($akGalleryLinkToCID) || !intval($akGalleryLinkToCID->getAttributeKeyID())) {
				$akGalleryLinkToCID = FileAttributeKey::add(
					$at,
					array(
						'akHandle' => $akHandle,
						'akName' => t('Gallery Link To Page'),
					),
					$pkg
				);
			}
		}
	}
	
}