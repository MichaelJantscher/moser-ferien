/**
 * @name		jQuery touchTouch plugin
 * @author		Martin Angelov
 * @version 	1.0
 * @url			http://tutorialzine.com/2012/04/mobile-touch-gallery/
 * @license		MIT License
 */

(function(){
	/* Creating the plugin */
	
	$.fn.touchTouch = function(options){
		var overlay = "",
		slider = "",
		description = "",
		close = "",
		showControls = "",
		prevArrow = "",
		nextArrow = "",
		desctext = "",
		overlayVisible = false,
		isTouchmove = false,
		timeout = null,
		initialDescBottom = 0,
		initialDescOpacity = 0,
		initialCloseTop = 0,
		initialCloseRight = 0,
		initialCloseOpacity = 0,
		initialControlsOpacity = 0,
		isIE = ie();	
		var settings = $.extend({
      	"bid": "",
      	"picClick": "close",
      	"showCaptions": 1,
      	"hideControls": 1,
      	"showClose": 1,
      	"hideDelay": 3000
    }, options);

		overlay = $('<div class="galleryOverlay" id="galleryOverlay'+settings.bid+'">');
		slider = $('<div class="gallerySlider" id="gallerySlider'+settings.bid+'">');
		if (settings.showCaptions === 1) {description = $('<p class="placeholderdesc" id="placeholderdesc'+settings.bid+'">');}
		if (settings.showClose === 1) {close = $('<a class="placeholderclose" id="placeholderclose'+settings.bid+'"></a>');}
		if (settings.hideControls === 1) {showControls = $('<a class="placeholdercontrols" id="placeholdercontrols'+settings.bid+'"></a>');}
		prevArrow = $('<a class="prevArrow" id="prevArrow'+settings.bid+'"></a>');
		nextArrow = $('<a class="nextArrow" id="nextArrow'+settings.bid+'"></a>');

		var placeholders = $([]),
			index = 0,
			items = this;
		
		// Appending the markup to the page
		overlay.hide().appendTo('body');
		slider.appendTo(overlay);
		if (settings.showCaptions === 1) {
			description.appendTo(overlay);
			initialDescBottom = parseInt(description.css("bottom"));
			initialDescOpacity = description.css("opacity");
		}
		
		if (settings.hideControls === 1) {
			overlay.append(showControls);
			initialControlsOpacity = showControls.css("opacity");
			showControls.click(function(e){
				e.preventDefault(); //rajoute parce que sinon le 'tap' event se lance 2 fois parait-il
				showHideControls();
			});
		};
		if (settings.showClose === 1) {
			overlay.append(close);
			initialCloseTop = parseInt(close.css("top"));
			initialCloseRight = parseInt(close.css("right"));
			initialCloseOpacity = close.css("opacity");
			close.click(function(e){
				e.preventDefault(); //rajoute parce que sinon le 'tap' event se lance 2 fois parait-il
				hideOverlay();
			});
		};
		// Creating a placeholder for each image
		items.each(function(){
			placeholders = placeholders.add($('<div class="placeholder">'));
		});
	
		// Hide the gallery if the background is touched / clicked
		slider.append(placeholders).bind('click', function(e){
			// alert(e.originalEvent.touches[0]);
			isTouchmove = true;
			e.preventDefault(); //rajoute parce que sinon le 'tap' event se lance 2 fois parait-il
			if($(e.target).is('img') && settings.picClick === 'close'){
				//faut-il rajouter 'tap' pour les touch screen puisque 'click' est dit etre lent?
				// console.log(e.target);
				hideOverlay();
				return;
			} else if($(e.target).is('img') && settings.picClick === 'next'){
				//faut-il rajouter 'tap' pour les touch screen puisque 'click' est dit etre lent?
				// console.log(e.target);
				showNext();
				return;
			}
				hideOverlay();
				return;
		});
		
		// Listen for touch events on the body and check if they
		// originated in #gallerySlider img - the images in the slider.
		$('body').delegate('.gallerySlider img', 'touchstart', function(e){
			isTouchmove = false;
			var touch = e.originalEvent,
				startX = touch.changedTouches[0].pageX;
			// alert(touch);
			slider.bind('touchmove',function(e){
				
				e.preventDefault();
				
				touch = e.originalEvent.touches[0] ||
						e.originalEvent.changedTouches[0];
				
				if(touch.pageX - startX > 10){
					isTouchmove = true;
					slider.unbind('touchmove');
					showPrevious();
					
					return false;
				}
				else if (touch.pageX - startX < -10){
					isTouchmove = true;
					slider.unbind('touchmove');
					showNext();
					
					return false;
				}
			});
			
			// Return false to prevent image 
			// highlighting on Android
			return false;
			
		}).bind('touchend',function(e){
			slider.unbind('touchmove');
			if ($(e.target).is('img') && isTouchmove == false){
				if(settings.picClick === 'close'){
					hideOverlay();
					return false;
				} else if(settings.picClick === 'next'){
					showNext();
					return false;
				}	
			}

		});
		
		// Listening for clicks on the thumbnails
		
		items.bind('click', function(e){
			if (isIE === undefined || isIE > 7) {
				e.preventDefault();
				
				// Find the position of this image
				// in the collection
				
				index = items.index(this);
				showOverlay(index);
				showImage(index);
				
				// Preload the next image
				preload(index+1);
				
				// Preload the previous
				preload(index-1);
			}
			
		});
		
		// If the browser does not have support 
		// for touch, display the arrows
		if ( !("ontouchstart" in window) ){
			overlay.append(prevArrow).append(nextArrow);
			
			prevArrow.click(function(e){
				e.preventDefault();
				showPrevious();
			});
			
			nextArrow.click(function(e){
				e.preventDefault();
				showNext();
			});
		}
		
		// Listen for arrow keys
		$(window).keydown(function(e){
		
			if (e.keyCode == 37){
				showPrevious();
			}
			else if (e.keyCode==39){
				showNext();
			} else if (e.keyCode == 27){
				hideOverlay();
			}
			//peut-etre rajouter escape pour fermer le slide?
		});
		
		
		/* Private functions */
		function ie (){
		 
		    var undef,
		        v = 3,
		        div = document.createElement('div'),
		        all = div.getElementsByTagName('i');
		 
		    while (
		        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
		        all[0]
		    );
		 
		    return v > 4 ? v : undef;
		 
		}
		function reposition(){
			// if (isScale == false){
			var topPos = 0,
			leftPos = 0,
			WST = $(window).scrollTop(),
			WSL = $(window).scrollLeft(),
			OOT = overlay.offset().top,
			OOL = overlay.offset().left;
			topPos = WST != OOT ? WST : 0;
			leftPos = WSL != OOL ? WSL : 0;
					
			overlay.css({'top': topPos, 'left': leftPos});
			// } else {
			// 	isScale = false;
			// }
			
		}
		function showOverlay(index){
			
			// If the overlay is already shown, exit
			if (overlayVisible){
				return false;
			}
			
			// Show the overlay
			overlay.show();
			reposition();
			setTimeout(function(){
				// Trigger the opacity CSS transition
				overlay.addClass('visible');
			}, 100);
	
			// Move the slider to the correct image
			offsetSlider(index);
			
			// Raise the visible flag
			overlayVisible = true;
		}
	
		function hideOverlay(){
			// If the overlay is not shown, exit
			if(!overlayVisible){
				return false;
			}
			
			// Hide the overlay
			overlay.hide().removeClass('visible');
			overlay.css({'top': 0, 'left': 0});
			overlayVisible = false;
		}
		function showHideControls() {

			if (timeout) {
				clearTimeout(timeout);
				timeout = null;
			}
			// move into view

			// if (settings.hideControls === 1) 
			if (desctext) {
				description.css({'bottom':'0', 'opacity':'1'});
			}
			if (settings.showClose === 1) {
				close.css({'top':initialCloseTop, 'right':initialCloseRight, 'opacity':initialCloseOpacity});
			}
			showControls.css({'top':'-45px', 'right':'-22px', 'opacity':'0'});
			// }
		// function() {
			timeout = setTimeout(function() {
				// Send back to original position
				timeout = null;
				if (desctext) {
					description.css({'bottom':'-40px', 'opacity':'0'});
				}
				if (settings.showClose === 1) {
					close.css({'top':'-55px', 'right':'-25px', 'opacity':'0'});
				}	
				showControls.css({'top':'0', 'right':'0', 'opacity':initialControlsOpacity});
			}, settings.hideDelay);
		// }
		}
		function offsetSlider(index){
			if (timeout) {
				clearTimeout(timeout);
				timeout = null;
			}
			// This will trigger a smooth css transition
			slider.css('left',(-index*100)+'%');
			
			desctext = items.eq(index).attr('data-tg-desc');
			
			if (desctext) {
			description.html(desctext);
			if (settings.hideControls === 1) {
				showHideControls();
			} else {
				//close already positione in css
				description.css({'bottom':'0', 'opacity':'1'});
				// showControls.css({'top':'-45px', 'right':'-22px', 'opacity':'0'});
			}
			// description.css({'bottom':'0', 'opacity':'1'});
			// description.animate({
			//     opacity: 1,
			//     bottom: 0
			//   }, 300 );	
			} else {
				if (settings.hideControls === 1) {
					showHideControls();
				} 
				if (settings.showCaptions === 1) {
					description.css({'bottom':'-40px', 'opacity':'0'});	
				}
				// showControls.css({'top':'-45px', 'right':'-22px', 'opacity':'0'});
				// description.animate({
			 //    opacity: 0,
			 //    bottom: "-=40px"
			 //  }, 300 );
			}
			
		}
	
		// Preload an image by its index in the items array
		function preload(index){
			setTimeout(function(){
				showImage(index);
			}, 1000);
		}
		
		// Show image in the slider
		function showImage(index){
	
			// If the index is outside the bonds of the array
			if(index < 0 || index >= items.length){
				return false;
			}
			
			// Call the load function with the href attribute of the item
			loadImage(items.eq(index).attr('href'), function(){
				placeholders.eq(index).html(this);
				// $('<p class="placeholderdesc">Test</p>').appendTo(placeholders.eq(index));
			});
		}
		
		// Load the image and execute a callback function.
		// Returns a jQuery object
		
		function loadImage(src, callback){
			var img = $('<img>').bind('load', function(){
				callback.call(img);
			});
			
			img.attr('src',src);
		}
		
		function showNext(){
			
			// If this is not the last image
			if(index+1 < items.length){
				index++;
				offsetSlider(index);
				preload(index+1);
			}
			else{
				// Trigger the spring animation
				
				slider.addClass('rightSpring');
				setTimeout(function(){
					slider.removeClass('rightSpring');
				},500);
			}
		}
		
		function showPrevious(){
			
			// If this is not the first image
			if(index>0){
				index--;
				offsetSlider(index);
				preload(index-1);
			}
			else{
				// Trigger the spring animation
				
				slider.addClass('leftSpring');
				setTimeout(function(){
					slider.removeClass('leftSpring');
				},500);
			}
		}
	};
	
})(jQuery);