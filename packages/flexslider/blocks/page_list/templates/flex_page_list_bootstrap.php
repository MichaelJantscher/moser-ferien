<?php   
	defined('C5_EXECUTE') or die("Access Denied.");
global $c;	
if ($c->isEditMode()) { 
	echo t('<h4><center>FlexSlider page list disabled on edit mode.<center></h4>');
}else{	
	
	$textHelper = Loader::helper("text");
	$imgHelper = Loader::Helper('image');
	// now that we're in the specialized content file for this block type, 
	// we'll include this block type's class, and pass the block to it, and get
	// the content
	
	if (count($cArray) > 0) { 

echo '<div id="flexslider-container'.$bID.'" class="default-flexslider-pglist">
<div class="flexslider-pg-container ">
<div class="flexslider-pg-c slides"> ';
	  
	for ($i = 0; $i < count($cArray); $i++ ) {
		$cobj = $cArray[$i]; 
		$target = $cobj->getAttribute('nav_target');
		$description = $cobj->getAttribute('meta-description'); // kg

		$title = $cobj->getCollectionName();
		$date = $cobj->getCollectionDatePublic('M j, Y'); ?>

	<div class="grid_4 main-content-thumb flex-slide">
	
	<div class="image-link">
	<a <?php    if ($target != '') { ?> target="<?php   echo $target?>" <?php    } ?> href="<?php   echo $nh->getLinkToCollection($cobj)?>">
		<?php   
		$imgnr=$cobj->getAttribute('flexslider_img');
		$imgnr2=$imgnr->fID;	
		if($imgnr2!=null){
		$showimg=$imgnr->getRelativePath();
		echo '<img src="'.$showimg.'" />';
		}
		?>
	</a>
	</div>
	<div class="flex-carousel-caption">
	<h4><a <?php    if ($target != '') { ?> target="<?php   echo $target?>" <?php    } ?> href="<?php   echo $nh->getLinkToCollection($cobj)?>"><?php   echo $title?></a></h4>
	<!-- <h3><a <?php    if ($target != '') { ?> target="<?php   echo $target?>" <?php    } ?> href="<?php   echo $nh->getLinkToCollection($cobj)?>"><?php   echo $textHelper->wordSafeShortText($title,$controller->truncateChars);?></a></h3> -->
	<p>
		<?php   
		if(!$controller->truncateSummaries){
			echo $cobj->getAttribute('meta_description');
			//echo $cobj->getCollectionDescription(); kg
		}else{
			echo $textHelper->wordSafeShortText($cobj->getAttribute('meta_description'),$controller->truncateChars);
			//echo $textHelper->wordSafeShortText($cobj->getCollectionDescription(),$controller->truncateChars); kg
		}
		?>
		<a <?php    if ($target != '') { ?> target="<?php   echo $target?>" <?php    } ?> href="<?php   echo $nh->getLinkToCollection($cobj)?>">more</a>
	</p>
	</div>
	</div>
	
<?php     } 

	?>
</div></div></div>
<?php 	}
}	?>