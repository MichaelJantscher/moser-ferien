<?php   
defined('C5_EXECUTE') or die("Access Denied.");
$url = parse_url($videoURL);
parse_str($url['query'], $query);
parse_str($url['path'], $path);
$c = Page::getCurrentPage();

if (!$vWidth) {
	$vWidth=425;
}
if (!$vHeight) {
	$vHeight=344;
}

if ($c->isEditMode()) { ?>
	<div class="ccm-edit-mode-disabled-item" style="width:<?php    echo $vWidth; ?>px; height:<?php    echo $vHeight; ?>px;">
		<div style="padding:8px 0px; padding-top: <?php    echo round($vHeight/2)-10; ?>px;"><?php    echo t('YouTube Video disabled in edit mode.'); ?></div>
	</div>
<?php    } elseif ($vPlayer==1) { ?>
	
	<div id="youtube<?php    echo $bID?>" class="youtubeBlock">
	<div id="youtube_target_<?php    echo $bID?>"></div>
	<?php    if($url['host'] == 'youtu.be') { 
	$y_link=$url['path']; 
	}else { 
	$y_link=$query['v']; 
	} ?>
	</div>

	<script>
	
	function onYouTubeIframeAPIReady() {
        youtube_custom_players[<?php    echo $bID?>] = new YT.Player('youtube_target_<?php    echo $bID?>', {
          height: '<?php    echo $vHeight?>',
          width: '<?php    echo $vWidth?>',
          videoId: '<?php    echo $y_link?>'
        });
       	<?php  
	global $yt_custom_arr;
	if(!empty($yt_custom_arr)){
	
	for($i=0;$i<count($yt_custom_arr);$i++)
		{
		echo "
		youtube_custom_players[".$yt_custom_arr[$i]['bid']."]=new YT.Player('youtube_target_".$yt_custom_arr[$i]['bid']."', {
		height: '".$yt_custom_arr[$i]['height']."',
          width: '".$yt_custom_arr[$i]['width']."',
          videoId: '".$yt_custom_arr[$i]['videoid']."'
        });
		";
		}
	}
	?> 
		
		
		
      }		  
	
	
	  </script>
	
	
<?php    } else { ?>
	
	<div class="ccm-edit-mode-disabled-item" style="width:<?php    echo $vWidth; ?>px; height:<?php    echo $vHeight; ?>px;">
		<div style="padding:8px 0px; padding-top: <?php    echo round($vHeight/2)-10; ?>px;"><?php    echo t('YouTube Video disabled'); ?></div>
	</div>
<?php    } ?>