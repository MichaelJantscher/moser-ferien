<?php   
defined('C5_EXECUTE') or die("Access Denied.");
$url = parse_url($videoURL);
parse_str($url['query'], $query);
parse_str($url['path'], $path);
$c = Page::getCurrentPage();

if (!$vWidth) {
	$vWidth=425;
}
if (!$vHeight) {
	$vHeight=344;
}

if ($c->isEditMode()) { ?>
	<div class="ccm-edit-mode-disabled-item" style="width:<?php    echo $vWidth; ?>px; height:<?php    echo $vHeight; ?>px;">
		<div style="padding:8px 0px; padding-top: <?php    echo round($vHeight/2)-10; ?>px;"><?php    echo t('YouTube Video disabled in edit mode.'); ?></div>
	</div>
<?php    } elseif ($vPlayer==1) { ?>
	
	<div id="youtube<?php    echo $bID?>" class="youtubeBlock">
	<div id="youtube_target_<?php    echo $bID?>"></div>
	<?php    if($url['host'] == 'youtu.be') { 
	$y_link=$url['path']; 
	}else { 
	$y_link=$query['v']; 
	} ?>
	</div>
	<?php   
	global $yt_custom_arr;
	$yt_obj_arr['height']=$vHeight;
	$yt_obj_arr['width']=$vWidth;
	$yt_obj_arr['videoid']=$y_link;
	$yt_obj_arr['bid']=$bID;
	$yt_custom_arr[]=$yt_obj_arr;

	} else { ?>
	
	<div class="ccm-edit-mode-disabled-item" style="width:<?php    echo $vWidth; ?>px; height:<?php    echo $vHeight; ?>px;">
		<div style="padding:8px 0px; padding-top: <?php    echo round($vHeight/2)-10; ?>px;"><?php    echo t('YouTube Video disabled'); ?></div>
	</div>
<?php    } ?>