<?php   
defined('C5_EXECUTE') or die("Access Denied.");
class FlexsliderBlockController extends BlockController {
	
	protected $btTable = 'btFlexslider';
	protected $btInterfaceWidth = "600";
	protected $btInterfaceHeight = "400";


	protected $btExportFileColumns = array('fID');
	protected $btExportTables = array('btFlexslider','btFlexsliderImg');

	public $defaultDuration = 5;	
	public $defaultFadeDuration = 2;	
	
	public $playback = "ORDER";	
	
	/** 
	 * Used for localization. If we want to localize the name/description we have to include this
	 */
	public function getBlockTypeDescription() {
		return t("Display a running loop of images.");
	}
	
	public function getBlockTypeName() {
		return t("FlexSlider");
	}
	
	public function getJavaScriptStrings() {
		return array(
			'choose-file' => t('Choose Image/File'),
			'choose-min-2' => t('Please choose at least two images.'),
			'choose-fileset' => t('Please choose a file set.')
		);
	}
	
	function getFileSetName(){
		$sql = "SELECT fsName FROM FileSets WHERE fsID=".intval($this->fsID);
		$db = Loader::db();
		return $db->getOne($sql); 
	}

	function loadFileSet(){
		if (intval($this->fsID) < 1) {
			return false;
		}
        Loader::helper('concrete/file');
		Loader::model('file_attributes');
		Loader::library('file/types');
		Loader::model('file_list');
		Loader::model('file_set');
		
		$ak = FileAttributeKey::getByHandle('height');

		$fs = FileSet::getByID($this->fsID);
		$fileList = new FileList();		
		$fileList->filterBySet($fs);
		$fileList->filterByType(FileType::T_IMAGE);	
		$fileList->sortByFileSetDisplayOrder();
		
		$files = $fileList->get(1000,0);
		
		
		$image = array();
		$image['duration'] = $this->duration;
		$image['fadeDuration'] = $this->fadeDuration;
		$image['groupSet'] = 0;
		$image['url'] = '';
		$images = array();
		$maxHeight = 0;
		foreach ($files as $f) {
			$fp = new Permissions($f);
			if(!$fp->canRead()) { continue; }
			$image['fID'] 			= $f->getFileID();
			$image['fileName'] 		= $f->getFileName();
			$image['fullFilePath'] 	= $f->getPath();
			$image['url']			= $f->getRelativePath();
			
			// find the max height of all the images so Flexslider doesn't bounce around while rotating
			$vo = $f->getAttributeValueObject($ak);
			if (is_object($vo)) {
				$image['imgHeight'] = $vo->getValue('height');
			}
			if ($maxHeight == 0 || $image['imgHeight'] > $maxHeight) {
				$maxHeight = $image['imgHeight'];
			}
			$images[] = $image;
		}
		$this->images = $images;
	
	}

	function loadImages(){
		if(intval($this->bID)==0) $this->images=array();
		$sortChoices=array('ORDER'=>'position','RANDOM-SET'=>'groupSet asc, position asc','RANDOM'=>'rand()');
		if( !array_key_exists($this->playback,$sortChoices) ) 
			$this->playback='ORDER';
		if(intval($this->bID)==0) return array();
		$sql = "SELECT * FROM btFlexsliderImg WHERE bID=".intval($this->bID).' ORDER BY '.$sortChoices[$this->playback];
		$db = Loader::db();
		$this->images=$db->getAll($sql); 
	}
	
	function delete(){
		$db = Loader::db();
		$db->query("DELETE FROM btFlexsliderImg WHERE bID=".intval($this->bID));		
		parent::delete();
	}
	
	function loadBlockInformation() {
		if ($this->fsID == 0) {
			$this->loadImages();
		} else {
			$this->loadFileSet();
		}
		$this->randomizeImages();	
		$this->set('defaultFadeDuration', $this->defaultFadeDuration);
		$this->set('defaultDuration', $this->defaultDuration);
		$this->set('fadeDuration', $this->fadeDuration);
		$this->set('duration', $this->duration);
		$this->set('minHeight', $this->minHeight);
		$this->set('fsID', $this->fsID);
		$this->set('fsName', $this->getFileSetName());
		$this->set('images', $this->images);
		$this->set('playback', $this->playback);
		$type = ($this->fsID > 0) ? 'FILESET' : 'CUSTOM';
		$this->set('type', $type);
		$this->set('bID', $this->bID);				
	}
	
	function view() {
		$this->loadBlockInformation();
	}

	function add() {
		$this->loadBlockInformation();
	}
	
	function edit() {
		$this->loadBlockInformation();
	}
	
	function duplicate($nbID) {
		parent::duplicate($nbID);
		$this->loadBlockInformation();
		$db = Loader::db();
		foreach($this->images as $im) {
			$db->Execute('insert into btFlexsliderImg (bID, fID, url, groupSet, position, imgdesc,hasContent,ctextarea) values (?, ?, ?, ?, ?, ?, ?, ?)', 
				array($nbID, $im['fID'], $im['url'], $im['groupSet'], $im['position'], $im['imgdesc'], $im['hasContent'], $im['ctextarea'])
			);		
		}
	}
	
	function save($data) { 
		$args['selector'] = trim($data['selector']);
		$args['animation'] = isset($data['animation']) ? trim($data['animation']) : 'fade';
		$args['direction'] = isset($data['direction']) ? trim($data['direction']) : 'horizontal';
		$args['reverse'] = isset($data['reverse']) ? trim($data['reverse']) : 'false';
		$args['animationLoop'] = isset($data['animationLoop']) ? trim($data['animationLoop']) : 'true';
		$args['smoothHeight'] = isset($data['smoothHeight']) ? trim($data['smoothHeight']) : 'false';		
		$args['startAt'] = isset($data['startAt']) ? trim($data['startAt']) : 0;
		$args['slideshow'] = isset($data['slideshow']) ? trim($data['slideshow']) : 'true';
		$args['slideshowSpeed'] = isset($data['slideshowSpeed']) ? trim($data['slideshowSpeed']) : 7000;
		$args['animationSpeed'] = isset($data['animationSpeed']) ? trim($data['animationSpeed']) : 600;
		$args['initDelay'] = isset($data['initDelay']) ? trim($data['initDelay']) : 0;
		$args['randomize'] = isset($data['randomize']) ? trim($data['randomize']) : 'false';
		$args['forcecrop'] = isset($data['forcecrop']) ? trim($data['forcecrop']) : 'true';		
		$args['pauseOnAction'] = isset($data['pauseOnAction']) ? trim($data['pauseOnAction']) : 'true';
		$args['pauseOnHover'] = isset($data['pauseOnHover']) ? trim($data['pauseOnHover']) : 'false';
		$args['useCSS'] = isset($data['useCSS']) ? trim($data['useCSS']) : 'true';
		$args['touch'] = isset($data['touch']) ? trim($data['touch']) : 'true';
		$args['video'] = isset($data['video']) ? trim($data['video']) : 'false';
		$args['controlNav'] = isset($data['controlNav']) ? trim($data['controlNav']) : 'true';
		$args['directionNav'] = isset($data['directionNav']) ? trim($data['directionNav']) : 'true';
		$args['prevText'] = isset($data['prevText']) ? trim($data['prevText']) : 'Previous';
		$args['nextText'] = isset($data['nextText']) ? trim($data['nextText']) : 'Next';
		$args['keyboard'] = isset($data['keyboard']) ? trim($data['keyboard']) : 'true';
		$args['multipleKeyboard'] = isset($data['multipleKeyboard']) ? trim($data['multipleKeyboard']) : 'false';
		$args['mousewheel'] = isset($data['mousewheel']) ? trim($data['mousewheel']) : 'false';
		$args['pausePlay'] = isset($data['pausePlay']) ? trim($data['pausePlay']) : 'false';
		$args['pauseText'] = isset($data['pauseText']) ? trim($data['pauseText']) : 'Pause';
		$args['playText'] = isset($data['playText']) ? trim($data['playText']) : 'Play';
		$args['controlsContainer'] = isset($data['controlsContainer']) ? trim($data['controlsContainer']) : '';
		$args['manualControls'] = isset($data['manualControls']) ? trim($data['manualControls']) : '';
		$args['sync'] = isset($data['sync']) ? trim($data['sync']) : '';
		$args['asNavFor'] = isset($data['asNavFor']) ? trim($data['asNavFor']) : '';
		$args['itemWidth'] = isset($data['itemWidth']) ? trim($data['itemWidth']) : 0;
		$args['itemMargin'] = isset($data['itemMargin']) ? trim($data['itemMargin']) : 0;
		$args['minItems'] = isset($data['minItems']) ? trim($data['minItems']) : 1;
		$args['maxItems'] = isset($data['maxItems']) ? trim($data['maxItems']) : 1;
		$args['move'] = isset($data['move']) ? trim($data['move']) : 0;		
		$args['maxwidth'] = isset($data['maxwidth']) ? trim($data['maxwidth']) : '0';
		$args['maxheight'] = isset($data['maxheight']) ? trim($data['maxheight']) : '450';		
		$args['cdnstatus'] = isset($data['cdnstatus']) ? trim($data['cdnstatus']):'false';		
		$args['cdnurl'] = isset($data['cdnurl']) ? trim($data['cdnurl']):'true';		
		$args['flextarget'] = isset($data['flextarget']) ? trim($data['flextarget']) : 'image';
		$args['layouttarget'] = isset($data['layouttarget']) ? trim($data['layouttarget']) : 'ccm-layout-main-1-1';
		$args['pglisttarget'] = isset($data['pglisttarget']) ? trim($data['pglisttarget']) : 'ccm-flex-pg-list-1';
		
		$args['prod_attr'] = isset($data['prod_attr']) ? trim($data['prod_attr']):'true';
		$args['akID'] = isset($data['akID']) ? trim($data['akID']) : 0;
		$args['PrOrderTag'] = isset($data['PrOrderTag']) ? trim($data['PrOrderTag']):'productID';
		$args['PrOrderDirection'] = isset($data['PrOrderDirection']) ? trim($data['PrOrderDirection']):'DESC';
		$args['PrOrderLimit'] = isset($data['PrOrderLimit']) ? trim($data['PrOrderLimit']):1;
		$args['containerwidth'] = isset($data['containerwidth']) ? trim($data['containerwidth']) : 0;
		$args['containerheight'] = isset($data['containerheight']) ? trim($data['containerheight']) : 0;
		$args['filesetextra'] = isset($data['filesetextra']) ? trim($data['filesetextra']) : 0;
		$db = Loader::db();
		
		if( $data['type'] == 'FILESET' && $data['fsID'] > 0){
			$args['fsID'] = $data['fsID'];
			$args['duration'] = $data['duration'][0];
			$args['fadeDuration'] = $data['fadeDuration'][0];

			$files = $db->getAll("SELECT fv.fID FROM FileSetFiles fsf, FileVersions fv WHERE fsf.fsID = " . $data['fsID'] .
			         " AND fsf.fID = fv.fID AND fvIsApproved = 1");
			
			//delete existing images
			$db->query("DELETE FROM btFlexsliderImg WHERE bID=".intval($this->bID));
		} else if( $data['type'] == 'CUSTOM' && count($data['imgFIDs']) ){
			$args['fsID'] = 0;

			//delete existing images
			$db->query("DELETE FROM btFlexsliderImg WHERE bID=".intval($this->bID));
			
			//loop through and add the images
			$pos=0;
			foreach($data['imgFIDs'] as $imgFID){ 
				if(intval($imgFID)==0 || $data['fileNames'][$pos]=='tempFilename') continue;
				
				$tcontent = $this->translateTo($data['ctextarea'][$pos]);
			
			parent::save($args);
				$vals = array(intval($this->bID),intval($imgFID), trim($data['url'][$pos]),intval($data['groupSet'][$pos]),trim($data['imgdesc'][$pos]),
						intval($data['hasContent'][$pos]),$tcontent,$pos);
				$db->query("INSERT INTO btFlexsliderImg (bID,fID,url,groupSet,imgdesc,hasContent,ctextarea,position) values (?,?,?,?,?,?,?,?)",$vals);
				$pos++;
			}
		}
		
		parent::save($args);
	}
	
	function randomizeImages()
	{
		if($this->playback == 'RANDOM')
		{
			shuffle($this->images);
		}
		else if($this->playback == 'RANDOM-SET')
		{
			$imageGroups=array();
			$imageGroupIds=array();
			$sortedImgs=array();
			foreach($this->images as $imgInfo){
				$imageGroups[$imgInfo['groupSet']][]=$imgInfo;
				if( !in_array($imgInfo['groupSet'],$imageGroupIds) )
					$imageGroupIds[]=$imgInfo['groupSet'];
			}
			shuffle($imageGroupIds);
			foreach($imageGroupIds as $imageGroupId){
				foreach($imageGroups[$imageGroupId] as $imgInfo)
					$sortedImgs[]=$imgInfo;
			}
			$this->images=$sortedImgs;
		}
	}
	
		
		function getContent() {
			$content = $this->translateFrom($this->content);
			return $content;				
		}
		
		public function getSearchableContent(){
			return $this->content;
		}
		
		function br2nl($str) {
			$str = str_replace("\r\n", "\n", $str);
			$str = str_replace("<br />\n", "\n", $str);
			return $str;
		}
		
		function getContentEditMode() {
			$content = $this->translateFromEditMode($this->content);
			return $content;				
		}

		public function getImportData($blockNode) {
			$args = array();
			$content = $blockNode->data->record->content;

			$content = preg_replace_callback(
				'/\{ccm:export:page:(.*)\}/i',
				array('ContentBlockController', 'replacePagePlaceHolderOnImport'),				
				$content);

			$content = preg_replace_callback(
				'/\{ccm:export:image:(.*)\}/i',
				array('ContentBlockController', 'replaceImagePlaceHolderOnImport'),				
				$content);

			$content = preg_replace_callback(
				'/\{ccm:export:file:(.*)\}/i',
				array('ContentBlockController', 'replaceFilePlaceHolderOnImport'),				
				$content);

			$content = preg_replace_callback(
				'/\{ccm:export:define:(.*)\}/i',
				array('ContentBlockController', 'replaceDefineOnImport'),				
				$content);

			$args['content'] = $content;			
			return $args;
		}
		
		public static function replacePagePlaceHolderOnImport($match) {
			$cPath = $match[1];
			if ($cPath) { 
				$pc = Page::getByPath($cPath);
				return '{CCM:CID_' . $pc->getCollectionID() . '}';
			} else {
				return '{CCM:CID_1}';
			}
		}

		public static function replaceDefineOnImport($match) {
			$define = $match[1];
			if (defined($define)) {
				$r = get_defined_constants();
				return $r[$define];
			}
		}

		public static function replaceImagePlaceHolderOnImport($match) {
			$filename = $match[1];
			$db = Loader::db();
			$fID = $db->GetOne('select fID from FileVersions where filename = ?', array($filename));
			return '{CCM:FID_' . $fID . '}';
		}
		
		public static function replaceFilePlaceHolderOnImport($match) {
			$filename = $match[1];
			$db = Loader::db();
			$fID = $db->GetOne('select fID from FileVersions where filename = ?', array($filename));
			return '{CCM:FID_DL_' . $fID . '}';
		}
		
		public function export(SimpleXMLElement $blockNode) {			
			
			$data = $blockNode->addChild('data');
			$data->addAttribute('table', $this->btTable);
			$record = $data->addChild('record');
			$content = $this->content;
			$content = preg_replace_callback(
				'/{CCM:CID_([0-9]+)}/i',
				array('ContentExporter', 'replacePageWithPlaceHolderInMatch'),				
				$content);

			$content = preg_replace_callback(
				'/{CCM:FID_([0-9]+)}/i',
				array('ContentExporter', 'replaceImageWithPlaceHolderInMatch'),				
				$content);

			$content = preg_replace_callback(
				'/{CCM:FID_DL_([0-9]+)}/i',
				array('ContentExporter', 'replaceFileWithPlaceHolderInMatch'),				
				$content);


			$record->addChild('content', '<![CDATA['.Loader::helper('text')->entities($content).']]>');
		}
		
		

		function translateFromEditMode($text) {
			// now we add in support for the links
			
			$text = preg_replace(
				'/{CCM:CID_([0-9]+)}/i',
				BASE_URL . DIR_REL . '/' . DISPATCHER_FILENAME . '?cID=\\1',
				$text);

			// now we add in support for the files
			
			$text = preg_replace_callback(
				'/{CCM:FID_([0-9]+)}/i',
				array('ContentBlockController', 'replaceFileIDInEditMode'),				
				$text);

			
			$text = preg_replace_callback(
				'/{CCM:FID_DL_([0-9]+)}/i',
				array('ContentBlockController', 'replaceDownloadFileIDInEditMode'),				
				$text);
			

			return $text;
		}
		
		function translateFrom($text) {
			// old stuff. Can remove in a later version.
			$text = str_replace('href="{[CCM:BASE_URL]}', 'href="' . BASE_URL . DIR_REL, $text);
			$text = str_replace('src="{[CCM:REL_DIR_FILES_UPLOADED]}', 'src="' . BASE_URL . REL_DIR_FILES_UPLOADED, $text);

			// we have the second one below with the backslash due to a screwup in the
			// 5.1 release. Can remove in a later version.

			$text = preg_replace(
				array(
					'/{\[CCM:BASE_URL\]}/i',
					'/{CCM:BASE_URL}/i'),
				array(
					BASE_URL . DIR_REL,
					BASE_URL . DIR_REL)
				, $text);
				
			// now we add in support for the links
			
			$text = preg_replace_callback(
				'/{CCM:CID_([0-9]+)}/i',
				array('ContentBlockController', 'replaceCollectionID'),				
				$text);

			$text = preg_replace_callback(
				'/<img [^>]*src\s*=\s*"{CCM:FID_([0-9]+)}"[^>]*>/i',
				array('ContentBlockController', 'replaceImageID'),				
				$text);

			// now we add in support for the files that we view inline			
			$text = preg_replace_callback(
				'/{CCM:FID_([0-9]+)}/i',
				array('ContentBlockController', 'replaceFileID'),				
				$text);

			// now files we download
			
			$text = preg_replace_callback(
				'/{CCM:FID_DL_([0-9]+)}/i',
				array('ContentBlockController', 'replaceDownloadFileID'),				
				$text);
			
			return $text;
		}
		
		private function replaceFileID($match) {
			$fID = $match[1];
			if ($fID > 0) {
				$path = File::getRelativePathFromID($fID);
				return $path;
			}
		}
		
		private function replaceImageID($match) {
			$fID = $match[1];
			if ($fID > 0) {
				preg_match('/width\s*="([0-9]+)"/',$match[0],$matchWidth);
				preg_match('/height\s*="([0-9]+)"/',$match[0],$matchHeight);
				$file = File::getByID($fID);
				if (is_object($file) && (!$file->isError())) {
					$imgHelper = Loader::helper('image');
					$maxWidth = ($matchWidth[1]) ? $matchWidth[1] : $file->getAttribute('width');
					$maxHeight = ($matchHeight[1]) ? $matchHeight[1] : $file->getAttribute('height');
					if ($file->getAttribute('width') > $maxWidth || $file->getAttribute('height') > $maxHeight) {
						$thumb = $imgHelper->getThumbnail($file, $maxWidth, $maxHeight);
						return preg_replace('/{CCM:FID_([0-9]+)}/i', $thumb->src, $match[0]);
					}
				}
				return $match[0];
			}
		}

		private function replaceDownloadFileID($match) {
			$fID = $match[1];
			if ($fID > 0) {
				$c = Page::getCurrentPage();
				if (is_object($c)) {
					return View::url('/download_file', 'view', $fID, $c->getCollectionID());
				} else {
					return View::url('/download_file', 'view', $fID);
				}
			}
		}

		private function replaceDownloadFileIDInEditMode($match) {
			$fID = $match[1];
			if ($fID > 0) {
				return View::url('/download_file', 'view', $fID);
			}
		}
		
		private function replaceFileIDInEditMode($match) {
			$fID = $match[1];
			return View::url('/download_file', 'view_inline', $fID);
		}
		
		private function replaceCollectionID($match) {
			$cID = $match[1];
			if ($cID > 0) {
				$c = Page::getByID($cID, 'APPROVED');
				return Loader::helper("navigation")->getLinkToCollection($c);
			}
		}
		
		function translateTo($text) {
			// keep links valid
			$url1 = str_replace('/', '\/', BASE_URL . DIR_REL . '/' . DISPATCHER_FILENAME);
			$url2 = str_replace('/', '\/', BASE_URL . DIR_REL);
			$url3 = View::url('/download_file', 'view_inline');
			$url3 = str_replace('/', '\/', $url3);
			$url3 = str_replace('-', '\-', $url3);
			$url4 = View::url('/download_file', 'view');
			$url4 = str_replace('/', '\/', $url4);
			$url4 = str_replace('-', '\-', $url4);
			$text = preg_replace(
				array(
					'/' . $url1 . '\?cID=([0-9]+)/i', 
					'/' . $url3 . '([0-9]+)\//i', 
					'/' . $url4 . '([0-9]+)\//i', 
					'/' . $url2 . '/i'),
				array(
					'{CCM:CID_\\1}',
					'{CCM:FID_\\1}',
					'{CCM:FID_DL_\\1}',
					'{CCM:BASE_URL}')
				, $text);
			return $text;
		}
		
		
}

?>
