<?php    defined('C5_EXECUTE') or die("Access Denied."); 
//common
global $c;
$ih = Loader::helper('image');

echo '<style >.flexslider_edit_disable{width:100%;min-height:20px;background:#999999;padding:10px;text-align:center;color:#fff}
.flexslider_edit_disable.error{color:#cf0000}
</style>';

if ($c->isEditMode()) { 
//disable in edit mode
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled in edit mode.</h4></div>');
}
else{ 
echo '<style>a:focus{outline:none!important;}</style>';

if($flextarget=='page_list'){

if($pglisttarget!=null){
if($itemWidth!=null && $itemWidth!=0 ){
if($maxItems!=null && $maxItems!=0){
$flexmaxwidth=$maxItems * $itemWidth;
$flexWidth=$flexmaxwidth.'px';}
else{
$flexWidth=$itemWidth.'px';}} else{$flexWidth='100%';}

$ltarget_arr=explode('_-0-_',$pglisttarget);
if(count($ltarget_arr)==2){
$page = Page::getCurrentPage();
$pid=$page->getCollectionID();
$sql='SELECT * FROM `CollectionVersionBlocks` WHERE arHandle ="'.$ltarget_arr[0].'" AND cbDisplayOrder='.$ltarget_arr[1].' AND cID='.$pid.'  ORDER BY cvID DESC LIMIT 1';

$db = Loader::db();
$layout_arr = $db->getAll($sql);
if($layout_arr !=null){
$sl='flexslider-container'.$layout_arr[0]['bID'];
echo '<style type="text/css">';

echo '
#'.$sl.' {max-width:'.$flexWidth.';position:relative;}
#'.$sl.' p {margin:0px!important;padding:0px!important;}
#'.$sl.' .flexslider-pg-c:after {content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0;} 
#'.$sl.' {margin:0 auto 40px auto}
#'.$sl.' .flexslider-pg-c .flex-slide{display:none }
#'.$sl.' .flexslider-pg-c .flex-slide:first-child {
 display:block
}';
if($itemMargin!=null){echo '#'.$sl.' .flex-slide {margin-right:'.$itemMargin.'px} #'.$sl.' {padding:'.$itemMargin.'px;padding-right:0px;}';}
echo '</style>';
echo '
<script type="text/javascript">
$(window).load(function() {';
echo '$("#'.$sl.'").flexslider({';
echo 'selector:".flexslider-pg-c > div ",';

if($animation!=null){ echo 'animation:"'.$animation.'",';}	
if($direction!=null && $animation!='fade'){ echo 'direction:"'.$direction.'",';}
if($reverse!=null && $animation!='fade'){ echo 'reverse:'.$reverse.',';}
if($animationLoop!=null){ echo 'animationLoop:'.$animationLoop.',';}
if($startAt	!=null){ echo 'startAt:'.$startAt.',';}
if($slideshow!=null){ echo 'slideshow :'.$slideshow.',';}
if($slideshowSpeed!=null){ echo 'slideshowSpeed :'.$slideshowSpeed.',';}
if($animationSpeed!=null){ echo 'animationSpeed :'.$animationSpeed.',';}
if($initDelay!=null){ echo 'initDelay :'.$initDelay.',';}
if($randomize!=null){ echo 'randomize :'.$randomize.',';}
if($pauseOnAction!=null){ echo 'pauseOnAction :'.$pauseOnAction.',';}
if($pauseOnHover!=null){ echo 'pauseOnHover :'.$pauseOnHover.',';}
if($smoothHeight!=null){ echo 'smoothHeight :'.$smoothHeight.',';}
if($useCSS!=null){ echo 'useCSS :'.$useCSS.',';}
if($touch!=null){ echo 'touch :'.$touch.',';}
if($video!=null){ echo 'video :'.$video.',';}
if($controlNav!=null){ echo 'controlNav :'.$controlNav.',';}
if($directionNav!=null){ echo 'directionNav :'.$directionNav.',';}
if($prevText!=null && $directionNav!=null && $directionNav!='false'){ echo 'prevText :"'.$prevText.'",';}
if($nextText!=null && $directionNav!=null && $directionNav!='false'){ echo 'nextText :"'.$nextText.'",';}
if($keyboard!=null){ echo 'keyboard :'.$keyboard.',';}
if($multipleKeyboard!=null){ echo 'multipleKeyboard :'.$multipleKeyboard.',';}
if($mousewheel!=null){ echo 'mousewheel :'.$mousewheel.',';}
if($pausePlay!=null){ echo 'pausePlay :'.$pausePlay.',';}
if($pauseText!=null && $pausePlay!=null && $pausePlay!='false'){ echo 'pauseText :"'.$pauseText.'",';}
if($playText!=null && $pausePlay!=null && $pausePlay!='false'){ echo 'playText :"'.$playText.'",';}
if($controlsContainer!=null){ echo 'controlsContainer :"'.$controlsContainer.'",';}
if($manualControls!=null){ echo 'manualControls :"'.$manualControls.'",';}
if($sync!=null){ echo 'sync :"'.$sync.'",';}
if($asNavFor!=null){ echo 'asNavFor :"'.$asNavFor.'",';}
if($itemWidth!=null && $itemWidth!=0 && $animation!='fade' && $direction!="vertical" && $minItems!=null && $minItems!=0 ){ echo 'itemWidth:'.$itemWidth.',';}
if($itemMargin!=null && $animation!='fade'&& $direction!="vertical"){ echo 'itemMargin:'.$itemMargin.',';}
if($minItems!=null && $animation!='fade'&& $direction!="vertical"  && $minItems!=0){ echo 'minItems:'.$minItems.',';}
if($maxItems!=null && $animation!='fade' && $direction!="vertical"  && $maxItems!=0){ echo 'maxItems :'.$maxItems.',';}
if($move!=null && $animation!='fade' && $direction!="vertical" && $move!=0){ echo 'move :'.$move.',';}

echo'
 namespace: "flex-"
	});
});
	</script>';
 if($maxwidth=="0"){$maxwidth='';}	
 if($maxheight=="0"){$maxheight='';}
 echo '<div style="width:100%;height:20px;display:block;clear:both;"></div>';
 }
 else{ echo t('<div class="flexslider_edit_disable error"><h4 >DB Error! Invalid page list!!!</h4></div>');}
}
else{
 echo t('<div class="flexslider_edit_disable error"><h4 >Error! Invalid page list!!!</h4></div>');
}
 }
 else{
 echo t('<div class="flexslider_edit_disable error"><h4 >Error! Invalid page list!!!</h4></div>');
 }
 
}

else{
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled. This custom template is for a page list slider only.</h4></div>');
}
}
?>