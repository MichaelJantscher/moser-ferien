<?php    defined('C5_EXECUTE') or die("Access Denied."); 
//common
global $c;
$ih = Loader::helper('image');
if($itemWidth!=null && $itemWidth!=0 ){$flexWidth=$itemWidth.'px';} else{$flexWidth='100%';}
echo '<style >.flexslider_edit_disable{width:100%;min-height:20px;background:#999999;padding:10px;text-align:center;color:#fff}
.flexslider_edit_disable.error{color:#cf0000}
a:focus{outline:none!important;}
</style>';


if ($c->isEditMode()) { 
//disable in edit mode
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled on edit mode.</h4></div>');
}
else{ 
//image type
if($flextarget=='image'){

echo'<style >';

echo '.flexslider-container'.$bID.' {margin:0 auto;position:relative;	}';
if($itemWidth!=null && $itemWidth!=0 &&$minItems!=null &&$minItems!=0 && $animation!='fade' && $direction!="vertical"){
$customwidth=$itemWidth-20;
if($maxItems!=null && $maxItems!=0){
$width2x=$maxItems*$itemWidth;
}
else{
$width2x=$minItems*$itemWidth;
}
echo '.flexslider-container'.$bID.' ul li .flex-caption {max-width:100%} .flexslider-container'.$bID.'{max-width:'.$width2x.'px}';
}


if($itemWidth!=null && $itemWidth!=0 && $direction!="vertical")
{
echo '.flexslider-container'.$bID.' img {max-width:'.$itemWidth.'px;}';
echo '@media (max-width: '.$itemWidth.'px) {.flexslider-container'.$bID.' img {max-width:100%!important}}';
}
if($maxheight!=null && $maxheight!=0 && $direction!="vertical")
{
echo '.flexslider-container'.$bID.' img {max-height:'.$maxheight.'px;}';
}

if($containerwidth!=null && $containerwidth!=0 )
{
echo '.external-flexslider-container'.$bID.'{max-width:'.$containerwidth.'px;margin:0 auto}';
}
if($containerheight!=null && $containerheight!=0 ){
echo '.external-flexslider-container'.$bID.'{max-height:'.$containerheight.'px;margin:0 auto}';
echo '.external-flexslider-container'.$bID.' ul.slides'.$bID.' li img{max-height:'.$containerheight.'px;margin:0 auto;}';
}
if($itemMargin!=null){echo '.flexslider-container'.$bID.' li {margin-right:'.$itemMargin.'px} .flexslider-container'.$bID.' {padding:'.$itemMargin.'px;padding-right:0px;} .flexslider-container'.$bID.' img {max-width:100%;}';}
echo '
.flexslider-container'.$bID.' {margin:0 auto 40px auto}
.flexslider-container'.$bID.' h1 {margin:0;font-size:35px;font-weight:normal;text-align:left;}
	</style>';
	
	
	
echo '<script type="text/javascript"> $(window).load(function() {';

echo '$(".flexslider-container'.$bID.'").flexslider({';
if($selector!=null && trim($selector)!=''){ echo 'selector:"'.$selector.'",';}
if($animation!=null){ echo 'animation:"'.$animation.'",';}	
if($direction!=null && $animation!='fade'){ echo 'direction:"'.$direction.'",';}
if($reverse!=null && $animation!='fade'){ echo 'reverse:'.$reverse.',';}
if($animationLoop!=null){ echo 'animationLoop:'.$animationLoop.',';}
if($startAt	!=null){ echo 'startAt:'.$startAt.',';}
if($slideshow!=null){ echo 'slideshow :'.$slideshow.',';}
if($slideshowSpeed!=null){ echo 'slideshowSpeed :'.$slideshowSpeed.',';}
if($animationSpeed!=null){ echo 'animationSpeed :'.$animationSpeed.',';}
if($initDelay!=null){ echo 'initDelay :'.$initDelay.',';}
if($randomize!=null){ echo 'randomize :'.$randomize.',';}
if($pauseOnAction!=null){ echo 'pauseOnAction :'.$pauseOnAction.',';}
if($pauseOnHover!=null){ echo 'pauseOnHover :'.$pauseOnHover.',';}
if($smoothHeight!=null){ echo 'smoothHeight :'.$smoothHeight.',';}
if($useCSS!=null){ echo 'useCSS :'.$useCSS.',';}
if($touch!=null){ echo 'touch :'.$touch.',';}
if($video!=null){ echo 'video :'.$video.',';}
if($controlNav!=null){ echo 'controlNav :'.$controlNav.',';}
if($directionNav!=null){ echo 'directionNav :'.$directionNav.',';}
if($prevText!=null && $directionNav!=null && $directionNav!='false'){ echo 'prevText :"'.$prevText.'",';}
if($nextText!=null && $directionNav!=null && $directionNav!='false'){ echo 'nextText :"'.$nextText.'",';}
if($keyboard!=null){ echo 'keyboard :'.$keyboard.',';}
if($multipleKeyboard!=null){ echo 'multipleKeyboard :'.$multipleKeyboard.',';}
if($mousewheel!=null){ echo 'mousewheel :'.$mousewheel.',';}
if($pausePlay!=null){ echo 'pausePlay :'.$pausePlay.',';}
if($pauseText!=null && $pausePlay!=null && $pausePlay!='false'){ echo 'pauseText :"'.$pauseText.'",';}
if($playText!=null && $pausePlay!=null && $pausePlay!='false'){ echo 'playText :"'.$playText.'",';}
if($controlsContainer!=null){ echo 'controlsContainer :"'.$controlsContainer.'",';}
if($manualControls!=null){ echo 'manualControls :"'.$manualControls.'",';}
if($sync!=null){ echo 'sync :"'.$sync.'",';}
if($asNavFor!=null){ echo 'asNavFor :"'.$asNavFor.'",';}
if($itemWidth!=null && $itemWidth!=0 && $animation!='fade' && $direction!="vertical" && $minItems!=null && $minItems!=0 ){ echo 'itemWidth:'.$itemWidth.',';}
if($itemMargin!=null && $animation!='fade'&& $direction!="vertical"){ echo 'itemMargin:'.$itemMargin.',';}
if($minItems!=null && $animation!='fade'&& $direction!="vertical"  && $minItems!=0){ echo 'minItems:'.$minItems.',';}
if($maxItems!=null && $animation!='fade' && $direction!="vertical"  && $maxItems!=0){ echo 'maxItems :'.$maxItems.',';}
if($move!=null && $animation!='fade' && $direction!="vertical" && $move!=0){ echo 'move :'.$move.',';}
echo '
 namespace: "flex-",
  start: function(){
 $(".flexslider-img-preloader").css("background-image","none");
 $(".flexslider-img-content").css("opacity","1");
  $(".flexslider-img-content").css("visibility","visible");
 }
	});
});
	</script>';
 if($maxwidth=="0"){$maxwidth='';}	
 if($maxheight=="0"){$maxheight='';}
echo'
<div class="external-flexslider-container'.$bID.' flexslider-img-preloader">
<div class="blackimg-flexslider flexslider-container'.$bID.' flexslider-img-content">
<ul class="slides'.$bID.' blackimg-flex-p-slides slides">';

	foreach($images as $imgInfo) {
	$f = File::getByID($imgInfo['fID']);
	$fp = new Permissions($f);
	if ($fp->canRead()) {	
		$imgpath=$f->getRelativePath();
		$img_path_r=$f->getRelativePath();
		$img_path_t=$img_path_r;
		if($itemWidth!=null && $itemWidth!='0' && $maxheight!=null && $maxheight!=0){
		if($forcecrop=='true'){
			$img_path_o=$ih->getThumbnail($f, $itemWidth, $maxheight, true);
				$img_path_t=$img_path_o->src;
		}
		else{$img_path_o=$ih->getThumbnail($f, $itemWidth, $maxheight, false);
		$img_path_t=$img_path_o->src;}		
			
		}
		if($cdnstatus=='true'){
			if($cdnurl){				
				$img_path_t=$cdnurl.$img_path_t;
			}
			
		}
		?>	
		<li>
		<?php  
		

		if($imgInfo['url']!=null && $fsID==0 ){?>
		
			<a href="<?php   echo $imgInfo['url']?>">	<img src="<?php   echo $img_path_t; ?>" /></a>
		<?php   }else{?>
			<img src="<?php   echo $img_path_t;?>" />
			
		<?php   }
		if($filesetextra==1 ||$filesetextra=='1' ){
		$img_title=$f->getTitle();
		$img_desc=$f->getDescription();
		
		}
		else{
		$img_title=$imgInfo['imgdesc'];
		$img_desc=$imgInfo['ctextarea'];
		}
		if($img_title!=null || $img_desc!=null ){
		
		echo '<div class="flex-caption">
		<h3>'.$img_title.'</h3>
		<p>'.$img_desc.'</p>
		</div>';
		
		}
		
		?>
		</li>
		<?php    
	}
	}
	echo '</ul>
 </div></div>
<div style="clear: both;  width: 100%; height: 20px;"></div>';	

}  

else{
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled.This custom template is for image list only.</h4></div>');
}	
}
?>