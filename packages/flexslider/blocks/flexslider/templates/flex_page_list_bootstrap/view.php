<?php   defined('C5_EXECUTE') or die("Access Denied."); 
global $c;
$ih = Loader::helper('image');
if($itemWidth!=null && $itemWidth!=0 ){$itemWidth2=$itemWidth.'px';} else{$itemWidth2='100%';}
if ($c->isEditMode()) { 
	echo t('<h4><center>FlexSlider disabled on edit mode.<center></h4>');
}else{
$page = Page::getCurrentPage();
$pid=$page->getCollectionID();
 
if($flextarget=='page_list'){
$ltarget_arr=explode('_-0-_',$pglisttarget);
$sql='SELECT * FROM `CollectionVersionBlocks` WHERE arHandle ="'.$ltarget_arr[0].'" AND cbDisplayOrder='.$ltarget_arr[1].' AND cID='.$pid.'  ORDER BY cvID DESC LIMIT 1';

$db = Loader::db();
$layout_arr = $db->getAll($sql);
$sl='flexslider-container'.$layout_arr[0]['bID'];

echo '<style type="text/css">

#'.$sl.' {height:auto;width:'.$itemWidth2.';position:relative;}
#'.$sl.' .flex-active-slide,#'.$sl.' .flex-slide{height:auto;overflow:hidden;}
#'.$sl.' .flexslider-pg-c:after {content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0;} 
#'.$sl.' {margin-bottom:60px}
#'.$sl.' .flexslider-pg-c .flex-slide{display:none }
#'.$sl.' .flexslider-pg-c .flex-slide:first-child {
 display:block
}

@media (min-width: 768px) {
#'.$sl.' {height:'.$maxheight.'px;}
#'.$sl.' .flex-active-slide,#'.$sl.' .flex-slide{height:'.$maxheight.'px;}}
</style>';

?>
<script type="text/javascript">
$(window).load(function() {
<?php 
echo '$("#'.$sl.'").flexslider({';
echo 'selector:".flexslider-pg-c > div ",';


//read flex options
if($animation!=null){ echo 'animation:"'.$animation.'",';}	
if($direction!=null){ echo 'direction:"'.$direction.'",';}
if($reverse!=null){ echo 'reverse:'.$reverse.',';}
if($animationLoop!=null){ echo 'animationLoop:'.$animationLoop.',';}
if($startAt	!=null){ echo 'startAt:'.$startAt.',';}
if($slideshow!=null){ echo 'slideshow :'.$slideshow.',';}
if($slideshowSpeed!=null){ echo 'slideshowSpeed :'.$slideshowSpeed.',';}
if($animationSpeed!=null){ echo 'animationSpeed :'.$animationSpeed.',';}
if($initDelay!=null){ echo 'initDelay :'.$initDelay.',';}
if($randomize!=null){ echo 'randomize :'.$randomize.',';}
if($pauseOnAction!=null){ echo 'pauseOnAction :'.$pauseOnAction.',';}
if($pauseOnHover!=null){ echo 'pauseOnHover :'.$pauseOnHover.',';}
if($useCSS!=null){ echo 'useCSS :'.$useCSS.',';}
if($touch!=null){ echo 'touch :'.$touch.',';}
if($video!=null){ echo 'video :'.$video.',';}
if($controlNav!=null){ echo 'controlNav :'.$controlNav.',';}
if($directionNav!=null){ echo 'directionNav :'.$directionNav.',';}
if($prevText!=null){ echo 'prevText :"'.$prevText.'",';}
if($nextText!=null){ echo 'nextText :"'.$nextText.'",';}
if($keyboard!=null){ echo 'keyboard :'.$keyboard.',';}
if($multipleKeyboard!=null){ echo 'multipleKeyboard :'.$multipleKeyboard.',';}
if($mousewheel!=null){ echo 'mousewheel :'.$mousewheel.',';}
if($pausePlay!=null){ echo 'pausePlay :'.$pausePlay.',';}
if($pauseText!=null){ echo 'pauseText :"'.$pauseText.'",';}
if($playText!=null){ echo 'playText :"'.$playText.'",';}
if($controlsContainer!=null){ echo 'controlsContainer :"'.$controlsContainer.'",';}
if($manualControls!=null){ echo 'manualControls :"'.$manualControls.'",';}
if($sync!=null){ echo 'sync :"'.$sync.'",';}
if($asNavFor!=null){ echo 'asNavFor :"'.$asNavFor.'",';}

if($itemMargin!=null){ echo 'itemMargin :'.$itemMargin.',';}
if($minItems!=null){ echo 'minItems :'.$minItems.',';}
if($maxItems!=null){ echo 'maxItems :'.$maxItems.',';}
if($move!=null){ echo 'move :'.$move.',';}

?>
 namespace: "flex-"
	});
});
	</script>
<?php  if($maxwidth=="0"){$maxwidth='';}?>	
<?php  if($maxheight=="0"){$maxheight='';}

}
else{
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled. This custom template is for a page list slider only.</h4></div>');
}	

}?>