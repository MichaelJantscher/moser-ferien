<?php    defined('C5_EXECUTE') or die("Access Denied."); 
//common
global $c;
$ih = Loader::helper('image');

echo '<style >.flexslider_edit_disable{width:100%;min-height:20px;background:#999999;padding:10px;text-align:center;color:#fff}
.flexslider_edit_disable.error{color:#cf0000}
</style>';

if ($c->isEditMode()) { 
//disable in edit mode
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled in edit mode.</h4></div>');
}
else{
if($itemWidth!=null && $itemWidth!=0 ){
if($maxItems!=null && $maxItems!=0){
$flexmaxwidth=$maxItems * $itemWidth;
$flexWidth=$flexmaxwidth.'px';}
else{
$flexWidth=$itemWidth.'px';}} else{$flexWidth='100%';}
$bt = BlockType::getByHandle('flexslider');
$basic_path=DIR_REL;
$basic_path=rtrim($basic_path,"/");
$burl_template=$basic_path.'/packages/flexslider/blocks/flexslider/templates/flex_layout_white/css/';

 

//image type
if($flextarget=='layout'){

$page = Page::getCurrentPage();
$pid=$page->getCollectionID();
if($layouttarget!=null){
$ltarget_arr=explode('_-0-_',$layouttarget);
if(count($ltarget_arr)==2){
$sql='SELECT * FROM `CollectionVersionAreaLayouts` WHERE arHandle ="'.$ltarget_arr[0].'" AND position='.$ltarget_arr[1].' AND cID ='.$pid.' ORDER BY cvID DESC LIMIT 1';
$db = Loader::db();
$layout_arr = $db->getAll($sql);
if($layout_arr!=null){
$sl='ccm-layout-wrapper-'.$layout_arr[0]['cvalID'];

echo '<style>';
echo '#'.$sl.' {display:block;background:#fff url('.$burl_template.'loading.gif) no-repeat;background-position:center center}';
echo '#'.$sl.' ul,#'.$sl.' ol, #'.$sl.' li{margin: 0; padding: 0; list-style: none;}';
echo '#'.$sl.' a:active,
.flex-viewport a:active,
#'.$sl.' a:focus,
.flex-viewport a:focus  {outline: none;}
#'.$sl.' {margin: 0; padding: 0; list-style: none;clear:both} 
#'.$sl.' .flex-viewport {margin: 0; padding: 0;}
#'.$sl.' {  border: 4px solid #fff; position: relative; -webkit-border-radius: 4px; -moz-border-radius: 4px; -o-border-radius: 4px; border-radius: 4px; -webkit-box-shadow: 0 1px 4px rgba(0,0,0,.2); zoom: 1; 
 -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);}

#'.$sl.' .flex-viewport .ccm-layout > li {display: none; -webkit-backface-visibility: hidden;}
#'.$sl.' .flex-pauseplay span {text-transform: capitalize;}
html[xmlns] #'.$sl.'  .ccm-layout {display: block;} 
* html .ccm-layout {height: 1%;}
.no-js #'.$sl.' .ccm-layout > li:first-child {display: block;}

#'.$sl.' .flex-viewport {margin: 0 0 0px;zoom: 1;}
#'.$sl.' .ccm-layout-wrapper{  position: relative;  }
#'.$sl.' .flex-viewport {max-height: 2000px; -webkit-transition: all 1s ease; -moz-transition: all 1s ease; transition: all 1s ease;}
#'.$sl.' .loading .flex-viewport {max-height: 300px;}
#'.$sl.' .flex-viewport .ccm-layout {zoom: 1;}
.carousel li {margin-right: 5px}
#'.$sl.' .flex-direction-nav a {width: 30px; height: 30px; margin: -20px 0 0; display: block; background: url('.$burl_template.'bg_direction_nav.png) no-repeat 0 0; position: absolute; top: 50%; cursor: pointer; text-indent: -9999px; opacity: 0; -webkit-transition: all .3s ease;}
#'.$sl.' .flex-direction-nav .flex-next {background-position: 100% 0; right: 1px; }
#'.$sl.' .flex-direction-nav .flex-prev {left: 1px;}
#'.$sl.':hover .flex-next {opacity: 0.8; }
#'.$sl.':hover .flex-prev {opacity: 0.8; }
#'.$sl.'  .flex-next:hover,#'.$sl.'  .flex-prev:hover {opacity: 1;}
#'.$sl.' .flex-direction-nav .disabled {opacity: .3!important; filter:alpha(opacity=30); cursor: default;}
#'.$sl.' .flex-control-nav {width: 100%; position: absolute; bottom: -40px; text-align: center;left:0;right:0;margin:0;padding:0;list-style:none}
#'.$sl.' .flex-control-nav ul{list-style:none;padding:0;margin:0}
#'.$sl.' .flex-control-nav li {margin: 0 0 0 5px; display: inline-block; zoom: 1; *display: inline;list-style:none;}
#'.$sl.' .flex-control-nav li:first-child {margin: 0;}
#'.$sl.' .flex-control-nav li a {width: 13px; height: 13px; display: block; background: url('.$burl_template.'bg_control_nav.png) no-repeat; cursor: pointer; text-indent: -999em;}
#'.$sl.' .flex-control-nav li a:hover {background-position: 0 -13px;}
#'.$sl.' .flex-control-nav li a.flex-active {background-position: 0 -26px; cursor: default;}


</style>';

echo '<style type="text/css">
.youtubeBlock, .vimeo-player{margin:0}
#'.$sl.' {max-width:'.$flexWidth.';position:relative;}
#'.$sl.' .flex-active-slide,#'.$sl.' .ccm-layout-row{}
#'.$sl.' .ccm-layout:after {content: "."; display: block; clear: both; visibility: hidden; line-height: 0; height: 0;} 
#'.$sl.' {margin:0 auto 40px auto}
#'.$sl.' .ccm-layout .ccm-layout-row{display:none }
#'.$sl.' .ccm-layout .ccm-layout-row:first-child {
 display:block
}';
if($itemMargin!=null){echo '#'.$sl.' .ccm-layout-row {margin-right:'.$itemMargin.'px} #'.$sl.' {padding:'.$itemMargin.'px;padding-right:0px;}';}
echo '#'.$sl.' .flex-viewport{opacity:0;visibility: hidden;}';
echo '</style>';

echo '
<script type="text/javascript">
$(window).load(function() { ';

echo '$("#'.$sl.'").flexslider({';
echo 'selector:".ccm-layout > div ",';
if($animation!=null){ echo 'animation:"'.$animation.'",';}	
if($direction!=null && $animation!='fade'){ echo 'direction:"'.$direction.'",';}
if($reverse!=null && $animation!='fade'){ echo 'reverse:'.$reverse.',';}
if($animationLoop!=null){ echo 'animationLoop:'.$animationLoop.',';}
if($startAt	!=null){ echo 'startAt:'.$startAt.',';}
if($slideshow!=null){ echo 'slideshow :'.$slideshow.',';}
if($slideshowSpeed!=null){ echo 'slideshowSpeed :'.$slideshowSpeed.',';}
if($animationSpeed!=null){ echo 'animationSpeed :'.$animationSpeed.',';}
if($initDelay!=null){ echo 'initDelay :'.$initDelay.',';}
if($randomize!=null){ echo 'randomize :'.$randomize.',';}
if($pauseOnAction!=null){ echo 'pauseOnAction :'.$pauseOnAction.',';}
if($pauseOnHover!=null){ echo 'pauseOnHover :'.$pauseOnHover.',';}
if($smoothHeight!=null){ echo 'smoothHeight :'.$smoothHeight.',';}
if($useCSS!=null){ echo 'useCSS :'.$useCSS.',';}
if($touch!=null){ echo 'touch :'.$touch.',';}
if($video!=null){ echo 'video :'.$video.',';}
if($controlNav!=null){ echo 'controlNav :'.$controlNav.',';}
if($directionNav!=null){ echo 'directionNav :'.$directionNav.',';}
if($prevText!=null && $directionNav!=null && $directionNav!='false'){ echo 'prevText :"'.$prevText.'",';}
if($nextText!=null && $directionNav!=null && $directionNav!='false'){ echo 'nextText :"'.$nextText.'",';}
if($keyboard!=null){ echo 'keyboard :'.$keyboard.',';}
if($multipleKeyboard!=null){ echo 'multipleKeyboard :'.$multipleKeyboard.',';}
if($mousewheel!=null){ echo 'mousewheel :'.$mousewheel.',';}
if($pausePlay!=null){ echo 'pausePlay :'.$pausePlay.',';}
if($pauseText!=null && $pausePlay!=null && $pausePlay!='false'){ echo 'pauseText :"'.$pauseText.'",';}
if($playText!=null && $pausePlay!=null && $pausePlay!='false'){ echo 'playText :"'.$playText.'",';}
if($controlsContainer!=null){ echo 'controlsContainer :"'.$controlsContainer.'",';}
if($manualControls!=null){ echo 'manualControls :"'.$manualControls.'",';}
if($sync!=null){ echo 'sync :"'.$sync.'",';}
if($asNavFor!=null){ echo 'asNavFor :"'.$asNavFor.'",';}
if($itemWidth!=null && $itemWidth!=0 && $animation!='fade' && $direction!="vertical" && $minItems!=null && $minItems!=0 ){ echo 'itemWidth:'.$itemWidth.',';}
if($itemMargin!=null && $animation!='fade'&& $direction!="vertical"){ echo 'itemMargin:'.$itemMargin.',';}
if($minItems!=null && $animation!='fade'&& $direction!="vertical"  && $minItems!=0){ echo 'minItems:'.$minItems.',';}
if($maxItems!=null && $animation!='fade' && $direction!="vertical"  && $maxItems!=0){ echo 'maxItems :'.$maxItems.',';}
if($move!=null && $animation!='fade' && $direction!="vertical" && $move!=0){ echo 'move :'.$move.',';}
echo '
 namespace: "flex-",
 start: function(){
 $("#'.$sl.'").css("background-image","none");
 $("#'.$sl.' .flex-viewport").css("opacity","1");
  $("#'.$sl.' .flex-viewport").css("visibility","visible");
 },
 before: function(slider){
	 if(typeof youtube_custom_players != "undefined" && typeof youtube_custom_players != "null")
	 {
	 for (var key in youtube_custom_players) {
			youtube_custom_players[key].stopVideo();
		}
	 }
 }
	});
});
	</script>';
 if($maxwidth=="0"){$maxwidth='';}
 if($maxheight=="0"){$maxheight='';}

echo '<div style="width:100%;height:20px;display:block;clear:both;"></div>';

}
else{
echo t('<div class="flexslider_edit_disable error"><h4 >DB Error! Invalid layout!!!</h4></div>');
}
}

else{
echo t('<div class="flexslider_edit_disable error"><h4 >Error! Invalid layout!!!</h4></div>');
}
}
else{
echo t('<div class="flexslider_edit_disable"><h4 >Please add a layout to the current page.</h4></div>');
}
}

else{
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled. This custom template is for a layout slider only.</h4></div>');
}
}
?>