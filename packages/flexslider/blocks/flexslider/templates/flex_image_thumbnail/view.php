<?php    defined('C5_EXECUTE') or die("Access Denied."); 
//common
global $c;
$ih = Loader::helper('image');
if($itemWidth!=null && $itemWidth!=0 ){$flexWidth=$itemWidth.'px';} else{$flexWidth='100%';}
echo '<style >.flexslider_edit_disable{width:100%;min-height:20px;background:#999999;padding:10px;text-align:center;color:#fff}
.flexslider_edit_disable.error{color:#cf0000}
</style>';

if ($c->isEditMode()) { 
//disable in edit mode
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled in edit mode.</h4></div>');
}
else{ 
echo '<style>a:focus{outline:none!important;}</style>';
//image type
if($flextarget=='image'){
echo'<style>';

echo '.flexslider-container'.$bID.' {margin:0 auto;position:relative;margin-bottom:40px	}';
if($itemWidth!=null && $itemWidth!=0 &&$minItems!=null &&$minItems!=0){
$customwidth=$itemWidth-20;
if($maxItems!=null && $maxItems!=0){
$width2x=$maxItems*$itemWidth;
}
else{
$width2x=$minItems*$itemWidth;
}
echo '.flexslider-container'.$bID.' ul li .flex-caption {max-width:100%} .flexslider-container'.$bID.'{max-width:'.$itemWidth.'px}';
}
echo '#carousel'.$bID.' {max-width:'.$itemWidth.'px;margin:5px auto;position:relative}';
echo '
.flexslider-container'.$bID.' {margin-bottom:0px}

.flexslider-container'.$bID.' h1 {margin:0;font-size:35px;font-weight:normal;text-align:left;}';

echo	'</style>';
echo '<script type="text/javascript"> $(window).load(function() {';
echo '
$("#carousel'.$bID.'").flexslider({
	selector: ".slides-thumb > li",
    animation: "slide",
    controlNav: false,';
if($animationLoop!=null){ echo 'animationLoop:'.$animationLoop.',';}else{echo 'animationLoop:true,';}
if($slideshow!=null){ echo 'slideshow :'.$slideshow.',';}
 echo   ' itemWidth: 210,
    itemMargin: 0,
 asNavFor: ".flexslider-container'.$bID.'"
  });



$(".flexslider-container'.$bID.'").flexslider({';
 echo 'animation: "slide",';
if($direction!=null){ echo 'direction:"'.$direction.'",';}
if($reverse!=null){ echo 'reverse:'.$reverse.',';}
if($animationLoop!=null){ echo 'animationLoop:'.$animationLoop.',';}
if($startAt	!=null){ echo 'startAt:'.$startAt.',';}
if($slideshow!=null){ echo 'slideshow :'.$slideshow.',';}
if($slideshowSpeed!=null){ echo 'slideshowSpeed :'.$slideshowSpeed.',';}
if($animationSpeed!=null){ echo 'animationSpeed :'.$animationSpeed.',';}
if($initDelay!=null){ echo 'initDelay :'.$initDelay.',';}
if($randomize!=null){ echo 'randomize :'.$randomize.',';}
if($pauseOnAction!=null){ echo 'pauseOnAction :'.$pauseOnAction.',';}
if($pauseOnHover!=null){ echo 'pauseOnHover :'.$pauseOnHover.',';}
if($useCSS!=null){ echo 'useCSS :'.$useCSS.',';}
if($touch!=null){ echo 'touch :'.$touch.',';}
if($video!=null){ echo 'video :'.$video.',';}
if($controlNav!=null){ echo 'controlNav :false,';}
if($directionNav!=null){ echo 'directionNav :'.$directionNav.',';}
if($prevText!=null){ echo 'prevText :"'.$prevText.'",';}
if($nextText!=null){ echo 'nextText :"'.$nextText.'",';}
if($keyboard!=null){ echo 'keyboard :'.$keyboard.',';}
if($multipleKeyboard!=null){ echo 'multipleKeyboard :'.$multipleKeyboard.',';}
if($mousewheel!=null){ echo 'mousewheel :'.$mousewheel.',';}
if($pausePlay!=null){ echo 'pausePlay :false,';}
if($pauseText!=null){ echo 'pauseText :"'.$pauseText.'",';}
if($playText!=null){ echo 'playText :"'.$playText.'",';}
if($controlsContainer!=null){ echo 'controlsContainer :"'.$controlsContainer.'",';}
if($manualControls!=null){ echo 'manualControls :"'.$manualControls.'",';}
echo 'sync :"#carousel'.$bID.'",';
if($itemWidth!=null && $itemWidth!=0){ echo 'itemWidth:'.$itemWidth.',';}

echo '
namespace: "flex-",
  start: function(){
 $(".flexslider-img-preloader").css("background-image","none");
 $(".flexslider-img-content").css("opacity","1");
  $(".flexslider-img-content").css("visibility","visible");
 }
	});
});
	</script>';
 if($maxwidth=="0"){$maxwidth='';}	
 if($maxheight=="0"){$maxheight='';}
echo'<div class="white-flexslider flexslider-container'.$bID.' flexslider-img-preloader">
<ul class="slides'.$bID.' white-flex-slides slides flexslider-img-content">';
 
	foreach($images as $imgInfo) {
	$f = File::getByID($imgInfo['fID']);
	$fp = new Permissions($f);
	if ($fp->canRead()) {	
		$imgpath=$f->getRelativePath();
		$img_path_r=$f->getRelativePath();
		$img_path_t=$img_path_r;
		if($itemWidth!=null && $itemWidth!='0' && $maxheight!=null && $maxheight!=0){
		if($forcecrop=='true'){
			$img_path_o=$ih->getThumbnail($f, $itemWidth, $maxheight, true);
			$img_path_t=$img_path_o->src;
		}
		else{$img_path_o=$ih->getThumbnail($f, $itemWidth, $maxheight, false);
		$img_path_t=$img_path_o->src;}		
			
		}
		if($cdnstatus=='true'){
			if($cdnurl){				
				$img_path_t=$cdnurl.$img_path_t;
			}
			
		}
		?>

<li>
  <?php  
		

		if($imgInfo['url']!=null && $fsID==0 ){?>
  <a href="<?php   echo $imgInfo['url']?>"> <img src="<?php   echo $img_path_t; ?>" /></a>
  <?php   }else{?>
  <img src="<?php   echo $img_path_t;?>" />
  <?php   }
		if($filesetextra==1 ||$filesetextra=='1' ){
		$img_title=$f->getTitle();
		$img_desc=$f->getDescription();
		
		}
		else{
		$img_title=$imgInfo['imgdesc'];
		$img_desc=$imgInfo['ctextarea'];
		}
		if($img_title!=null || $img_desc!=null ){
		
		echo '<div class="flex-caption">
		<h3>'.$img_title.'</h3>
		<p>'.$img_desc.'</p>
		</div>';
		
		}
		
		?>
</li>
<?php    
	}
	}
	echo '</ul>
 </div>
 <div id="carousel'.$bID.'" class="flex-control-thumbs white-flexslider flexslider-img-preloader">
<ul class="slides-thumb flexslider-img-content">';


foreach($images as $imgInfo) {
	$f = File::getByID($imgInfo['fID']);
	$fp = new Permissions($f);
	if ($fp->canRead()) {	
		$imgpath=$f->getRelativePath();
		$img_path_r=$f->getRelativePath();
		$img_path_t=$img_path_r;
		$img_path_o=$ih->getThumbnail($f,210, 130, true);
		$img_path_t=$img_path_o->src;	
		
		if($cdnstatus=='true'){
			if($cdnurl){				
				$img_path_t=$cdnurl.$img_path_t;
			}
			
		}
?>
<li> <img src="<?php   echo $img_path_t;?>" /> </li>
<?php   	
}}

echo '</ul>
</div>

<div style="clear: both;  width: 100%; height: 20px;"></div>';	

}  

else{
echo t('<div class="flexslider_edit_disable"><h4 >FlexSlider disabled. This custom template is for a thumbnail navigation slider only.</h4></div>');
}

}
?>
