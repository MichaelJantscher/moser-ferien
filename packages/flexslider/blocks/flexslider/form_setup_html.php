<?php  
defined('C5_EXECUTE') or die("Access Denied.");
$al = Loader::helper('concrete/asset_library');
$ah = Loader::helper('concrete/interface');
$fm = loader::helper('form');
$bt = BlockType::getByHandle('flexslider');
$basic_path=DIR_REL;
$basic_path=rtrim($basic_path,"/");
$burl=$basic_path.'/packages/flexslider/blocks/flexslider';

if ($startAt == null) {$startAt = 0;
}
if ($slideshowSpeed == null) {$slideshowSpeed = 7000;
}
if ($animationSpeed == null) {$animationSpeed = 600;
}
if ($initDelay == null) {$initDelay = 0;
}

if ($itemWidth == null) {$itemWidth = 0;
}
if ($itemMargin == null) {$itemMargin = 0;
}
if ($containerwidth == null) {$containerwidth = 0;
}
if ($containerheight == null) {$containerheight = 0;
}
if ($minItems == null) {$minItems = 1;
}
if ($maxItems == null) {$maxItems = 1;
}
if ($move == null) {$move = 0;
}
if ($maxheight == null) {$maxheight = 450;
}
if ($PrOrderLimit == null) {$PrOrderLimit = 1;
}
?>
<link href="<?php   echo $burl; ?>/bootstrap.css" rel="stylesheet">
<style>
#ccm-slideshowBlock-imgRows a{cursor:pointer}
#ccm-slideshowBlock-imgRows .ccm-slideshowBlock-imgRow,
#ccm-slideshowBlock-fsRow {margin-bottom:16px;clear:both;padding:7px;background-color:#eee}
#ccm-slideshowBlock-imgRows .ccm-slideshowBlock-imgRow a.moveUpLink{ display:block; background:url(<?php   echo $burl; ?>/images/arrow_up.png) no-repeat center; height:10px; width:16px; }
#ccm-slideshowBlock-imgRows .ccm-slideshowBlock-imgRow a.moveDownLink{ display:block; background:url(<?php   echo $burl; ?>/images/arrow_down.png) no-repeat center; height:10px; width:16px; }
#ccm-slideshowBlock-imgRows .ccm-slideshowBlock-imgRow a.moveUpLink:hover{background:url(<?php   echo $burl; ?>/images/arrow_up_black.png) no-repeat center;}
#ccm-slideshowBlock-imgRows .ccm-slideshowBlock-imgRow a.moveDownLink:hover{background:url(<?php   echo $burl; ?>/images/arrow_down_black.png) no-repeat center;}
#ccm-slideshowBlock-imgRows .cm-slideshowBlock-imgRowIcons{ float:right; width:35px; text-align:left; }
 .ccm-summary-selected-item input{margin-bottom:5px}

.ui-tabs .ui-tabs-nav li{margin:0 5px 0 0;padding:0}
 .ui-tabs .ui-tabs-nav li a{padding:0 5px;margin:0} 
div.ccm-block-field-group{border:none}
.ccm-summary-selected-item p{font-weight:bold}
textarea.ctextarea{width:450px;height:100px}
.ccm-ui .tooltip,.tooltip{opacity:1!important;display:inline-block}
</style>
<script type="text/javascript" src="<?php   echo $burl; ?>/jquery.ui.tabs.js"></script>
<script type="text/javascript" src="<?php   echo $burl; ?>/bootstrap-tooltip.js"></script>
<script type="text/javascript" src="<?php   echo $burl; ?>/bootstrap-popover.js"></script>

<script type="text/javascript">

	$(document).ready(function(){

			$('.tooltip').hover(function()

			{

			$(this).popover('show')

		});

		
		});

	$(function() {
		$( "#tabs" ).tabs({ selected: 0 });
	});
	
	$(function() {
		$( "#containerwidth" ).slider({
			value:<?php   echo $containerwidth; ?>,
			min: 0,
			max: 1500,
			step: 1,
			slide: function( event, ui ) {
				$( "#containerwidth_i" ).val(  ui.value );
			}
		});
		$( "#containerwidth_i" ).val( $( "#containerwidth" ).slider( "value" ) );
	});	
	
	$(function() {
		$( "#containerheight" ).slider({
			value:<?php   echo $containerheight; ?>,
			min: 0,
			max: 1500,
			step: 1,
			slide: function( event, ui ) {
				$( "#containerheight_i" ).val(  ui.value );
			}
		});
		$( "#containerheight_i" ).val( $( "#containerheight" ).slider( "value" ) );
	});	
		
	$(function() {
		$( "#PrOrderLimit" ).slider({
			value:<?php   echo $PrOrderLimit; ?>,
			min: 1,
			max: 50,
			step: 1,
			slide: function( event, ui ) {
				$( "#PrOrderLimit_i" ).val(  ui.value );
			}
		});
		$( "#PrOrderLimit_i" ).val( $( "#PrOrderLimit" ).slider( "value" ) );
	});	
		
	$(function() {
		$( "#slideshowSpeed" ).slider({
			value:<?php   echo $slideshowSpeed; ?>,
			min: 1000,
			max: 10000,
			step: 100,
			slide: function( event, ui ) {
				$( "#slideshowSpeed_i" ).val(  ui.value );
			}
		});
		$( "#slideshowSpeed_i" ).val( $( "#slideshowSpeed" ).slider( "value" ) );
	});	
	
	$(function() {
		$( "#animationSpeed" ).slider({
			value:<?php   echo $animationSpeed; ?>,
			min: 100,
			max: 5000,
			step: 10,
			slide: function( event, ui ) {
				$( "#animationSpeed_i" ).val(  ui.value );
			}
		});
		$( "#animationSpeed_i" ).val( $( "#animationSpeed" ).slider( "value" ) );
	});	
		
	$(function() {
		$( "#initDelay" ).slider({
			value:<?php   echo $initDelay; ?>,
			min: 0,
			max: 5000,
			step: 1,
			slide: function( event, ui ) {
				$( "#initDelay_i" ).val(  ui.value );
			}
		});
		$( "#initDelay_i" ).val( $( "#initDelay" ).slider( "value" ) );
	});	
		
	$(function() {
		$( "#itemWidth" ).slider({
			value:<?php   echo $itemWidth; ?>,
			min: 0,
			max: 1200,
			step: 1,
			slide: function( event, ui ) {
				$( "#itemWidth_i" ).val(  ui.value );
			}
		});
		$( "#itemWidth_i" ).val( $( "#itemWidth" ).slider( "value" ) );
	});	
	
	$(function() {
		$( "#itemMargin" ).slider({
			value:<?php   echo $itemMargin; ?>,
			min: 0,
			max: 500,
			step: 1,
			slide: function( event, ui ) {
				$( "#itemMargin_i" ).val(  ui.value );
			}
		});
		$( "#itemMargin_i" ).val( $( "#itemMargin" ).slider( "value" ) );
	});	
	
	$(function() {
		$( "#maxheight" ).slider({
			value:<?php   echo $maxheight; ?>
			, min: 0,
			max: 1200,
			step: 1,
			slide: function(event, ui ) {
				$("#maxheight_i").val(ui.value);
			}
			});
			$("#maxheight_i").val($("#maxheight").slider("value"));
			});
	
	</script>
	
	
<div class="ccm-ui">
<div class="ccm-block-field-group" id="tabs">	
<ul id="ccm-pagelist-tabs" class="ccm-dialog-tabs">
	<li ><a  href="#tabs-1"> <?php   echo t('Content') ?> </a></li>	
	<li ><a   href="#tabs-2"> <?php   echo t('Controls 1')?> </a></li>	
	<li ><a   href="#tabs-3"> <?php   echo t('Controls 2')?> </a></li>	
	<li ><a   href="#tabs-5"> <?php   echo t('Animation')?> </a></li>
	<li class="just_slide just_horizontal"><a   href="#tabs-4"> <?php   echo t('Multi')?> </a></li>
	<li ><a   href="#tabs-6"> <?php   echo t('Advanced  ')?> </a></li>
	<li class="cdn_area"><a   href="#tabs-7"> <?php   echo t('CDN Options')?> </a></li>
</ul>
<div style="clear:both"></div>
<div id="tabs-1" class="ccm-pagelistPane">
	
<div class="ccm-block-field-group" id="flex_select_type">
<strong><?php   echo t('Slider type')?></strong>
<select name="flextarget"  id="flextarget">
<option value="image" <?php  
	if ($flextarget == 'image') {echo 'selected';
	}
?>  ><?php   echo t('image')?></option>	
<option value="layout" <?php  
	if ($flextarget == 'layout') {echo 'selected';
	}
?>  ><?php   echo t('layout')?></option>	
<option value="page_list" <?php  
	if ($flextarget == 'page_list') {echo 'selected';
	}
?>  ><?php   echo t('page list')?></option>	
<option value="product" <?php  
	if ($flextarget == 'product') {echo 'selected';
	}
?>  ><?php   echo t('product list')?></option>		
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Image sliders provide ability to slide images or file sets.<br/><br/>Layout sliders provide ability to slide any type of block within a layout.<br/><br/>Page list sliders provide ability to slide a page list. (composer supported)<br/><br/>Product list sliders provide ability to slide eCommerce products if installed.')?>" data-original-title="<?php   echo t('Slider Type')?>" />

</div>
<div id="img_type_area">

<div id="newImg">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr>
	<td>
	<strong><?php   echo t('Image slider type')?></strong>
	<select name="type" style="vertical-align: middle">
		<option value="CUSTOM"<?php    if ($type == 'CUSTOM') { ?> selected<?php    } ?>><?php   echo t('image')?></option>
		<option value="FILESET"<?php    if ($type == 'FILESET') { ?> selected<?php    } ?>><?php   echo t('file set')?></option>
	</select>
	
	</td>
	
	</tr>
	<tr style="padding-top: 8px">
	<td colspan="2">
	<br />
	<span id="ccm-slideshowBlock-chooseImg"><?php   echo $ah -> button_js(t('Add Image'), 'SlideshowBlock.chooseImg()', 'left'); ?>
</span>
	
	</td>
	</tr>
	</table>
</div>
<br/>
<?php  echo t('Enable file name and descriptions from File Manager'); ?>
&nbsp;&nbsp;<input type="checkbox" value="1" name="filesetextra"  <?php  if($filesetextra=='1'){echo 'checked';} ?> /><br/>

<div id="ccm-slideshowBlock-imgRows">
<?php  
	if ($fsID <= 0) {
		foreach ($images as $imgInfo) {
			$f = File::getByID($imgInfo['fID']);
			$fp = new Permissions($f);
			$imgInfo['thumbPath'] = $f -> getThumbnailSRC(1);
			$imgInfo['fileName'] = $f -> getTitle();
			$imgInfo['slideshowImgId'] = $imgInfo['fID'];
			if ($fp -> canRead()) {
				$this -> inc('image_row_include.php', array('imgInfo' => $imgInfo));
			}
		}
	}
 ?>
</div>

<?php  
	Loader::model('file_set');
	$s1 = FileSet::getMySets();
	$sets = array();
	foreach ($s1 as $s) {
		$sets[$s -> fsID] = $s -> fsName;
	}
	$fsInfo['fileSets'] = $sets;

	if ($fsID > 0) {
		$fsInfo['fsID'] = $fsID;
		$fsInfo['duration'] = $duration;
		$fsInfo['fadeDuration'] = $fadeDuration;
	} else {
		$fsInfo['fsID'] = '0';
		$fsInfo['duration'] = $defaultDuration;
		$fsInfo['fadeDuration'] = $defaultFadeDuration;
	}
	$this -> inc('fileset_row_include.php', array('fsInfo' => $fsInfo));
 ?> 

<div id="imgRowTemplateWrap" style="display:none">
<?php  
	$imgInfo['slideshowImgId'] = 'tempSlideshowImgId';
	$imgInfo['fID'] = 'tempFID';
	$imgInfo['hasContent'] = 'temphasContent';
	$imgInfo['fileName'] = 'tempFilename';
	$imgInfo['origfileName'] = 'tempOrigFilename';
	$imgInfo['thumbPath'] = 'tempThumbPath';
	$imgInfo['duration'] = $defaultDuration;
	$imgInfo['fadeDuration'] = $defaultFadeDuration;
	$imgInfo['groupSet'] = 0;
	$imgInfo['imgHeight'] = tempHeight;
	$imgInfo['url'] = '';
	$imgInfo['class'] = 'ccm-slideshowBlock-imgRow';
?>
<?php    $this -> inc('image_row_include.php', array('imgInfo' => $imgInfo)); ?> 
</div>
<br/>

</div>

<div id="layout_type_area">

<div class=" alert-message info">
<?php    echo t('Layout type selected.'); ?>
</div>
<?php  
//show the list of layouts on this page
$page = Page::getCurrentPage();
$pid=$page->getCollectionID();
$db = Loader::db();
$lastver = $db->getAll("SELECT `cvID`  FROM `CollectionVersions` WHERE `cID`=".$pid." ORDER by `cvID` DESC LIMIT 1");
$lastver=$lastver[0]['cvID'];
$files = $db->getAll("SELECT * FROM `CollectionVersionAreaLayouts` WHERE `cID`=".$pid." AND `cvID`=".$lastver." ORDER by `cvID` DESC");

echo 'Layout area name:<br/>';
echo '<select name="layouttarget">';
foreach($files as $fslider){
$slidecontent="ccm-layout-wrapper";
?>
<option value="<?php   echo $fslider['arHandle'] . '_-0-_' . $fslider['position']; ?>" <?php    if ($layouttarget == $fslider['arHandle'].'_-0-_'.$fslider['position']) { ?> selected <?php    } ?>><?php   echo "area " . $fslider['arHandle'] . " at position " . $fslider['position']; ?> </option>
<?php  
}
echo '</select>';
?>
 <img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Select the name of the layout area that you would like to slide.<br/><br/>A layout must already be added to the page to become available as a selection in the drop down.')?>" data-original-title="<?php   echo t('Layout area name')?>" />


</div>
<div id="page_list_type_area">
<div class=" alert-message info">
<?php    echo t('Page list type selected.'); ?>
</div>
<?php  
$page = Page::getCurrentPage();
$pid = $page -> getCollectionID();
$db = Loader::db();

$pg_list_ver_arr = $db -> getAll("SELECT CollectionVersionBlocks.cvID,CollectionVersionBlocks.bID,CollectionVersionBlocks.arHandle ,CollectionVersionBlocks.cbDisplayOrder FROM CollectionVersionBlocks , btPageList WHERE CollectionVersionBlocks.cID=" . $pid . " AND CollectionVersionBlocks.bID= btPageList.bID AND CollectionVersionBlocks.cvID =" . $lastver . " ORDER by bID DESC ");
echo '<select name="pglisttarget">';
foreach ($pg_list_ver_arr as $arr) {
	$slidecontent = $arr['arHandle'] . '_-0-_' . $arr['cbDisplayOrder'];
	$userlist = $arr['cbDisplayOrder'] + 1;
	if ($pglisttarget == $slidecontent) {$selected = "selected";
	} else {$selected = '';
	}
	echo '<option value="' . $arr['arHandle'] . '_-0-_' . $arr['cbDisplayOrder'] . '" ' . $selected . ' >area ' . $arr['arHandle'] . ' at position ' . $userlist . '</option>';
}

echo '</select>';
?>
 <img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Select the area and position of the page list that you would like to slide.<br/><br/>A page list block must already be added to the page to become available as a selection in the drop down.')?>" data-original-title="<?php   echo t('Page List Selection')?>" />

</div>

<div id="product_type_area">
</div>
</div>

<div id="tabs-2" class="ccm-pagelistPane" >
<div class=" ccm-summary-selected-item">
<p><?php   echo t('Slider width')?>  <input type="text" id="itemWidth_i"  name="itemWidth"/> px 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('If using multiple images/elements per slide, this setting will be used for each image/element.<br/><br/> Setting to 0 will disable.')?>" data-original-title="<?php   echo t('Slider Width')?>" />
</p> 
<div id="itemWidth"></div>
</div>

<div class=" ccm-summary-selected-item">
<p><?php   echo t('Slider height')?>  <input type="text" id="maxheight_i"  name="maxheight"/> px
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('If using multiple images/elements per slide, this setting will apply to each image/element.<br/><br/> Setting to 0 will disable.')?>" data-original-title="<?php   echo t('Slider Height')?>" />

</p> 
<div id="maxheight"></div>
</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Force crop images')?></p>
<select name="forcecrop"  id="forcecrop">
	<option value="true" <?php  
	if ($forcecrop == 'true') {echo 'selected';
	}
?>  >true</option>
	<option value="false" <?php  
		if ($forcecrop == 'false') {echo 'selected';
		}
	?>  >false</option>	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('This setting applies to image sliders.<br/><br/>Set to true to crop images to the same aspect ratio. Set to false to keep images their default aspect ratio.')?>" data-original-title="<?php   echo t('Force Crop Images')?>" />

</div>

<div class=" ccm-summary-selected-item">
<p><?php   echo t('Item/image margin')?>  <input type="text" id="itemMargin_i"  name="itemMargin"/> px
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Adds spacing to each image/element of the slider.')?>" data-original-title="<?php   echo t('Item/image margin')?>" />

</p> 
<div id="itemMargin"></div>
</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('The number of slide the slider should start on.')?></p>
<?php    echo $fm->text('startAt',$startAt)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Enter the number of slide to start on. Array notation (0 = first slide)')?>" data-original-title="<?php   echo t('Start Number')?>" />

</div>


<div class="ccm-summary-selected-item">
<p ><?php    echo t('Randomize slide order')?></p>
<select name="randomize"  id="randomize">
<option value="false" <?php  
	if ($randomize == 'false') {echo 'selected';
	}
?>  >false</option>
	<option value="true" <?php  
	if ($randomize == 'true') {echo 'selected';
	}
?>  >true</option>	
	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Set to true to randomize order of slide playback. Set to false to play slides in sequential order.')?>" data-original-title="<?php   echo t('Randomize slide order')?>" />

</div>


<div class="ccm-summary-selected-item">
<p ><?php    echo t('Pause slideshow when interacting with control elements, highly recommended.')?></p>
<select name="pauseOnAction"  id="pauseOnAction">
<option value="false" <?php  
	if ($pauseOnAction == 'false') {echo 'selected';
	}
?>  >false</option>
	<option value="true" <?php  
	if ($pauseOnAction == 'true') {echo 'selected';
	}
?>  >true</option>
	
	
</select>

</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Pause slideshow when hovering, resume when no longer hovering')?></p>
<select name="pauseOnHover"  id="pauseOnHover">
<option value="false" <?php  
	if ($pauseOnHover == 'false') {echo 'selected';
	}
?>  >false</option>
	<option value="true" <?php  
	if ($pauseOnHover == 'true') {echo 'selected';
	}
?>  >true</option>	
	
</select>

</div>
</div>

<div id="tabs-3" class="ccm-pagelistPane" >

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Allow touch swipe navigation on touch-enabled devices')?></p>
<select name="touch"  id="touch">
<option value="true" <?php  
	if ($touch == 'true') {echo 'selected';
	}
?>  >true</option>
<option value="false" <?php  
	if ($touch == 'false') {echo 'selected';
	}
?>  >false</option>
		
	
</select>

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Prevent CSS3 3D Transforms. (If sliding video, set to true to avoid graphical glitches)')?></p>
<select name="video"  id="video">

	<option value="true" <?php  
	if ($video == 'true') {echo 'selected';
	}
?>  >true</option>	
<option value="false" <?php  
	if ($video == 'false') {echo 'selected';
	}
?>  >false</option>	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('If sliding video inside of a layout slider, set this to true.')?>" data-original-title="<?php   echo t('Prevent CSS3 3D Transforms')?>" />

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Create navigation for paging control of each slide')?></p>
<select name="controlNav"  id="controlNav">
<option value="true" <?php  
	if ($controlNav == 'true') {echo 'selected';
	}
?>  >true</option>
<option value="false" <?php  
	if ($controlNav == 'false') {echo 'selected';
	}
?>  >false</option>	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Set to true to display bullet point navigation below slider.')?>" data-original-title="<?php   echo t('Paging Control')?>" />

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Allow slider navigating via keyboard left/right keys')?></p>
<select name="keyboard"  id="keyboard">
<option value="true" <?php  
	if ($keyboard == 'true') {echo 'selected';
	}
?>  >true</option>	
<option value="false" <?php  
	if ($keyboard == 'false') {echo 'selected';
	}
?>  >false</option>	
</select>

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Allow keyboard navigation to affect multiple sliders')?></p>
<select name="multipleKeyboard"  id="multipleKeyboard">

<option value="true" <?php  
	if ($multipleKeyboard == 'true') {echo 'selected';
	}
?>  >true</option>	
<option value="false" <?php  
	if ($multipleKeyboard == 'false') {echo 'selected';
	}
?>  >false</option>	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Set to true to have keyboard control multiple sliders on the same page. Leaving set on false cuts out keyboard navigation with more than 1.')?>" data-original-title="<?php   echo t('Keyboard Navigation')?>" />

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Allows slider navigating with mousewheel')?></p>
<select name="mousewheel"  id="mousewheel">

<option value="true" <?php  
	if ($mousewheel == 'true') {echo 'selected';
	}
?>  >true</option>
<option value="false" <?php  
	if ($mousewheel == 'false') {echo 'selected';
	}
?>  >false</option>	
</select>

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Create pause/play dynamic element')?></p>
<select name="pausePlay"  id="pausePlay">
<option value="false" <?php  
	if ($pausePlay == 'false') {echo 'selected';
	}
?>  >false</option>
<option value="true" <?php  
	if ($pausePlay == 'true') {echo 'selected';
	}
?>  >true</option>	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Setting to true will display pause/play')?>" data-original-title="<?php   echo t('Pause/Play')?>" />


<p ><?php    echo t('Enter text for "pause"')?></p>
<?php    echo $fm->text('pauseText',$pauseText)?>
 

<p ><?php    echo t('Enter text for "play"')?></p>
<?php    echo $fm->text('playText',$playText)?>
 
</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Create navigation for previous/next navigation')?></p>
<select name="directionNav"  id="directionNav">
<option value="false" <?php  
	if ($directionNav == 'false') {echo 'selected';
	}
?>  >false</option>
<option value="true" <?php  
	if ($directionNav == 'true') {echo 'selected';
	}
?>  >true</option>	

</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Set to true to display left and right arrow navigation on slider.')?>" data-original-title="<?php   echo t('Previous/Next')?>" />


<p ><?php    echo t('Set text for "previous" direction')?></p>
<?php    echo $fm->text('prevText',$prevText)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Enter text to display for previous navigation text. This setting is optional.')?>" data-original-title="<?php   echo t('Previous Text')?>" />


<p ><?php    echo t('Set text for "next" direction')?></p>
<?php    echo $fm->text('nextText',$nextText)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Enter text to display for next navigation text. This setting is optional')?>" data-original-title="<?php   echo t('Next Text')?>" />

</div>


</div>

<div id="tabs-4" class="ccm-pagelistPane just_slide just_horizontal" >

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Minimum number of carousel items that should be visible. Items will resize fluidly when below this number.')?></p>
<?php    echo $fm->text('minItems',$minItems)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('This settings is used if you would like to display multiple images, pagelists, product lists or layouts within the same slider window.<br/><br/>Supports slide animation only.')?>" data-original-title="<?php   echo t('Carousel Items Min')?>" />


</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Maximum number of carousel items that should be visible. Items will resize fluidly when above this limit.')?></p>
<?php    echo $fm->text('maxItems',$maxItems)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('This settings is used if you would like to display multiple images, pagelists, product lists or layouts within the same slider window.<br/><br/>Supports slide animation only.')?>" data-original-title="<?php   echo t('Carousel Items Max')?>" />

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Number of carousel items that should move on animation. If set at 0, slider will move all visible items.')?></p>
<?php    echo $fm->text('move',$move)?>

</div>

</div>

<div id="tabs-5" class="ccm-pagelistPane" >

<div class="ccm-summary-selected-item animation_selector">
<p ><?php    echo t('Slider animation type')?></p>
<select name="animation"  id="animation">
	
	<option value="slide" <?php  
		if ($animation == 'slide') {echo 'selected';
		}
	?>  >slide</option>	
	<option value="fade" <?php  
	if ($animation == 'fade') {echo 'selected';
	}
?>  >fade</option>
</select>

</div>
<div class="ccm-summary-selected-item just_slide direction_selector">
<p ><?php    echo t('Sliding direction')?></p>
<select name="direction"  id="direction">
	<option value="horizontal" <?php  
	if ($direction == 'horizontal') {echo 'selected';
	}
?>  >horizontal</option>
	<option value="vertical" <?php  
		if ($direction == 'vertical') {echo 'selected';
		}
	?>  >vertical</option>	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Horzontial animates right to left.<br/><br/>Vertical animates top to bottom.')?>" data-original-title="<?php   echo t('Sliding Direction')?>" />

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Animate slider automatically')?></p>
<select name="slideshow"  id="slideshow">
	<option value="true" <?php  
	if ($slideshow == 'true') {echo 'selected';
	}
?>  >true</option>	
	<option value="false" <?php  
		if ($slideshow == 'false') {echo 'selected';
		}
	?>  >false</option>
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Set to true to automatically play slider when page loads.')?>" data-original-title="<?php   echo t('Auto Animation')?>" />

</div>

<div class=" ccm-summary-selected-item">
<p><?php   echo t('Set the speed of the slideshow cycling, in milliseconds')?>  <input type="text" id="slideshowSpeed_i"  name="slideshowSpeed"/>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Set the transition speed of the slide show in milliseconds.<br/><br/>1000 milliseconds = 1 second.')?>" data-original-title="<?php   echo t('Slideshow Speed')?>" />
</p> 
<div id="slideshowSpeed"></div>
</div>

<div class=" ccm-summary-selected-item">
<p><?php   echo t('Set speed of slide or fade animation, in milliseconds')?>  <input type="text" id="animationSpeed_i"  name="animationSpeed"/>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Sets the speed fade or slide animation takes to complete in milliseconds.<br/><br/>1000 milliseconds = 1 second')?>" data-original-title="<?php   echo t('Animation Speed')?>" />
</p> 
<div id="animationSpeed"></div>
</div>

<div class=" ccm-summary-selected-item">
<p><?php   echo t('Set the speed for play initialization delay, in milliseconds')?>  <input type="text" id="initDelay_i"  name="initDelay"/>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Set the time in which you would like the slider to start playing after the page loads in milliseconds. Only works if animate slider automatically is set to true.<br/><br/>1000 milliseconds = 1 second.')?>" data-original-title="<?php   echo t('Play Initialization')?>" />

</p> 
<div id="initDelay"></div>
</div>



<div class="ccm-summary-selected-item just_slide">
<p ><?php    echo t('Reverse animation direction')?></p>
<select name="reverse"  id="reverse">
	<option value="false" <?php  
	if ($reverse == 'false') {echo 'selected';
	}
?>  >false</option>
	<option value="true" <?php  
		if ($reverse == 'true') {echo 'selected';
		}
	?>  >true</option>	
</select>

</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Loop animation')?></p>
<select name="animationLoop"  id="animationLoop">
	<option value="true" <?php  
	if ($animationLoop == 'true') {echo 'selected';
	}
?>  >true</option>	
	<option value="false" <?php  
		if ($animationLoop == 'false') {echo 'selected';
		}
	?>  >false</option>
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('If set to false, nav will stop at each end of the slider.')?>" data-original-title="<?php   echo t('Loop Animation')?>" />

</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Allow height of slider to animate smoothly in horizontal mode')?></p>
<select name="smoothHeight"  id="smoothHeight">
	<option value="false" <?php  
	if ($smoothHeight == 'false') {echo 'selected';
	}
?>  >false</option>
	<option value="true" <?php  
		if ($smoothHeight == 'true') {echo 'selected';
		}
	?>  >true</option>	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Adjusts the height of slider automatically if images or content in each slide has a different height.<br/><br/> Supports fade animation only.')?>" data-original-title="<?php   echo t('Auto Height')?>" />

</div>
<div class="ccm-summary-selected-item">
<p ><?php    echo t('Use CSS3 transitions if available')?></p>
<select name="useCSS"  id="useCSS">
<option value="true" <?php  
	if ($useCSS == 'true') {echo 'selected';
	}
?>  >true</option>	
<option value="false" <?php  
	if ($useCSS == 'false') {echo 'selected';
	}
?>  >false</option>	
</select>
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('This setting can be utilized if you have created a custom template for flexslider that supports CSS3 transitions.<br/><br/>By default this is set to true.')?>" data-original-title="<?php   echo t('CSS3 Transitions')?>" />

</div>

</div>

<div id="tabs-6" class="ccm-pagelistPane" >

<div class=" ccm-summary-selected-item just_img">
<p><?php   echo t('Container width')?>  <input type="text" id="containerwidth_i"  name="containerwidth"/> px 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Supports image and file set slider types only.<br/><br/>Uses CSS to adjust container width.<br/><br/>Setting to 0 will disable.')?>" data-original-title="<?php   echo t('Container Width')?>" />
</p> 
<div id="containerwidth"></div>
</div>

<div class=" ccm-summary-selected-item just_img">
<p><?php   echo t('Container height')?>  <input type="text" id="containerheight_i"  name="containerheight"/> px
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Supports image and file set slider types only.<br/><br/>Uses CSS to adjust container height.<br/><br/>Setting to 0 will disable.')?>" data-original-title="<?php   echo t('Container Height')?>" />

</p> 
<div id="containerheight"></div>
</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Selector: ')?></p>
<?php    echo $fm->text('selector',$selector)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Must match a simple pattern. container > slide . Ignore pattern at your own peril .Default .slides > li '); ?>" data-original-title="<?php   echo t('Selector')?>" />
</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Controls container')?></p>
<?php    echo $fm->text('controlsContainer',$controlsContainer)?>
 <img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element.<br/><br/>Example use would be .flexslider-container<br/><br/>Property is ignored if given element is not found.')?>" data-original-title="<?php   echo t('Controls Container')?>" />

</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Manual controls')?></p>
<?php    echo $fm->text('manualControls',$manualControls)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('jQuery Object/Selector: Declare custom control navigation.<br/><br/> Examples would be<br/><br/>.flex-control-nav li or #tabs-nav li img, etc.<br/><br/>The number of elements in your controlNav should match the number of slides/tabs.')?>" data-original-title="<?php   echo t('Manual Controls')?>" />
</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Mirror the actions performed on this slider with another slider.')?></p>
<?php    echo $fm->text('sync',$sync)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Enter the id or class of the target slider.')?>" data-original-title="<?php   echo t('Mirror Actions')?>" />
</div>

<div class="ccm-summary-selected-item">
<p ><?php    echo t('Internal property exposed for turning the slider into a thumbnail navigation for another slider')?></p>
<?php    echo $fm->text('asNavFor',$asNavFor)?> 
<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Enter the id or class of the slider that you would like to act as a navigation for another slider.')?>" data-original-title="<?php   echo t('Thumbnail Navigation')?>" />
</div>


</div>

<div id="tabs-7" class="ccm-pagelistPane cdn_area" >
	<div class="ccm-summary-selected-item">
		<p><?php   echo t('Enable CDN')?></p>
		<select name="cdnstatus"  id="cdnstatus">
		<option value="false" <?php  
			if ($cdnstatus == 'false') {echo 'selected';
			}
		?>  >false</option>	
		<option value="true" <?php  
			if ($cdnstatus == 'true') {echo 'selected';
			}
		?>  >true</option>	
		</select>
		<img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Supports CDNs that offer a URL. Set to true to enable CDN functionality. Supports image and file set slider types only.')?>" data-original-title="<?php   echo t('Enable CDN')?>" />

	</div>

	<div class="ccm-summary-selected-item">
		<p><?php   echo t('CDN URL')?></p>
		<?php    echo $fm->text('cdnurl',$cdnurl)?>
 <img src="<?php   echo $burl; ?>/images/194.png"  width=16 height=16  class="tooltip" rel="tooltip" data-content="<?php   echo t('Enter the URL for your CDN. Include http:// but do not include a trailing slash /<br/><br/>Example: http://cdn.yourdomain.com')?>" data-original-title="<?php   echo t('CDN URL')?>" />
		
	</div>
</div>
	
</div>
</div>
