<?php   defined('C5_EXECUTE') or die(_("Access Denied."));

class FlexsliderPackage extends Package {

     protected $pkgHandle = 'flexslider';
     protected $appVersionRequired = '5.5.1';
     protected $pkgVersion = '2.1.1';

     public function getPackageDescription() {
          return t("An awesome, fully responsive jQuery slider addon with touch ability for mobile devices.");
     }

     public function getPackageName() {
          return t("FlexSlider");
     }
	 
	 public function upgrade() {
	 $pkg = parent::upgrade();
	 	$ift = AttributeType::getByHandle('image_file');
	 $flexslider_imgAttribute=CollectionAttributeKey::getByHandle('flexslider_img');
   if( !is_object($flexslider_imgAttribute) )
        CollectionAttributeKey::add($ift, array('akHandle' => 'flexslider_img', 'akName' => t('Flexslider page list img'), 'akIsSearchable' => false, 'akCheckedByDefault' => true), $pkg);
	 }
    
     public function install() {
          $pkg = parent::install(); 
		Loader::model('collection_types');
		BlockType::installBlockTypeFromPackage('flexslider', $pkg);	
	$ift = AttributeType::getByHandle('image_file');
	 $flexslider_imgAttribute=CollectionAttributeKey::getByHandle('flexslider_img');
   if( !is_object($flexslider_imgAttribute) )
        CollectionAttributeKey::add($ift, array('akHandle' => 'flexslider_img', 'akName' => t('Flexslider page list img'), 'akIsSearchable' => false, 'akCheckedByDefault' => true), $pkg);
     }
   
}
?>