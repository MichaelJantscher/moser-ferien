// JavaScript Document

jQuery.expr[':'].contains = function(a, i, m) {
  return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};

$(function() {
	$('input[id="quick_search_element"]').on('keydown, keyup', function() {
		var s = $(this).val();
		$(".searchable_elements li").show();
		if (s.length > 0)
			$(".searchable_elements li:not(:contains('"+s+"'))").hide();	
	});
	$("#ccm-pane-body-left li").click(function(){
		ccmFormidableOpenElementDialog($(this).attr('label'), $(this).text(), $(this).attr('data-layout'));
	});
});

ccmFormidableOpenNewElementDialog = function (layout_id) {
	jQuery.fn.dialog.closeTop();
	$("#ccm-pane-body-left li").attr('data-layout', layout_id);
	$('#quick_search').val('');
	$(".searchable_elements li").show();
	jQuery.fn.dialog.open({ 
		width: 900, height: 340, modal: true, element: '#ccm-pane-body-left', title: element_message_add		
	});
}
ccmFormidableOpenLayoutDialog = function(layoutID, rowID) {
	jQuery.fn.dialog.closeTop();
	var query_string = "formID="+formID+"&rowID="+rowID+"&layoutID="+layoutID;
	jQuery.fn.dialog.open({
		width: 670, height: 230, modal: true, href: layout_url+"?"+query_string, 
		title: (rowID < 0 ? layout_message_add : layout_message_edit)		
	});
}
ccmFormidableCheckFormLayoutSubmit = function() {
	var data = $('#layoutForm').serialize();
	data += '&action=save&formID='+formID;
	$.ajax({ 
		type: "POST",
		url: layout_tools_url,
		data: data,
		dataType: 'json',
		beforeSend: function () {
			ccmFormidableLoadLoader(true);
		},
		success: function(ret) {
			ccmFormidableLoadMessage('save_layout');
			ccmFormidableLoadElements();
			jQuery.fn.dialog.closeTop();
		}
	});	
}
ccmFormidableDeleteLayout = function(layoutID, rowID) {
	data = 'action=delete&layoutID='+layoutID+'&rowID='+rowID+'&formID='+formID;
	$.ajax({ 
		type: "POST",
		url: layout_tools_url,
		data: data,
		dataType: 'json',
		beforeSend: function () {
			ccmFormidableLoadLoader(true);
		},
		success: function(ret) {
			ccmFormidableLoadMessage('delete_layout');
			ccmFormidableLoadElements();
		}
	});	
}