<?php  
	
	defined('C5_EXECUTE') or die("Access Denied.");
	
	$json = Loader::helper('json');
	$cnt = Loader::controller('/dashboard/formidable/results');			
	
	switch ($_REQUEST['action'])
	{	
		case 'delete':
		case 'delete_multiple':		
			$r = array('type' => 'error',
					   'message' => t('Error: Result can\'t be deleted'));
			$return = $cnt->delete();
			if ($return)
				$r = array('type' => 'info',
						   'message' => t('Result succesfully deleted'));			
			echo $json->encode($r);
		break;
	}
?>