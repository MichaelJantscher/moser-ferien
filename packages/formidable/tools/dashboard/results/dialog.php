<?php   

	defined('C5_EXECUTE') or die("Access Denied.");
	
	$concrete_interface = Loader::helper('concrete/interface');
	$lh = Loader::helper('link', 'formidable');
	
	$cnt = Loader::controller('/dashboard/formidable/results');		
	$f = $cnt->getResult();	
		
    if (sizeof($f->results) <= 0) { 
?>
	<div class="ccm-ui element-body">
     <div class="message alert-message error dialog_message">
      <?php  echo t('There are no results found, the answers are empty or not set') ?>
     </div>
    </div>
    <div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix ccm-ui">
     <a href="javascript:void(0)" class="btn" onclick="ccm_blockWindowClose()"><?php   echo t('Cancel')?></a>
    </div>  
<?php   
	die(); }
?>
	<div class="ccm-ui element-body" id="ccm-mailing-results">
    
    <?php 
	$tabs = array(
		array('elements', t('Submitted data'), true),
		array('details', t('Submission details'))
	);
	// Print tab element
	echo Loader::helper('concrete/interface')->tabs($tabs);
	?>
    
	<div id="ccm-tab-content-elements" class="ccm-tab-content">
    <fieldset>
    <table width="100%" class="entry result_entry ccm-results-list">
     <tr>
      <th class="result_dialog_label"><?php   echo t('Label'); ?></th>
      <th><?php   echo t('Value'); ?></th>
     </tr>
     <?php  foreach ($f->elements as $element) { ?>
     <?php  if ($element->is_layout) continue; ?>
     <tr class="ccm-list-record">
      <td class="result_dialog_label"><?php  echo $element->label; ?></td>
      <td><?php  echo $lh->url_and_email_ify($element->result); ?></td>      
     </tr>
     <?php  } ?>
    </table>
	</fieldset>    
    </div>
    <div id="ccm-tab-content-details" class="ccm-tab-content">    
	<fieldset>
    <table width="100%" class="entry ccm-results-list">
     <tr>
      <th class="result_dialog_label"><?php   echo t('Label'); ?></th>
      <th><?php   echo t('Value'); ?></th>
     </tr>
     <tr class="ccm-list-record">
      <td class="result_dialog_label"><?php   echo t('Submitted on') ?></td>
      <td><?php   echo $f->results->submitted; ?></td>      
     </tr> 
     <tr class="ccm-list-record">
      <td class="result_dialog_label"><?php   echo t('From page') ?></td>
      <td>
      <?php  
		$p = Page::getById($f->results->collectionID);
		if (intval($p->getCollectionID()) != 0) { 
			echo '<a href="'.BASE_URL.DIR_REL.View::url($p->getCollectionPath()).'" target="_blank">'.$p->getCollectionName().'</a> ';			
			echo t('(Page ID: %s)', $p->getCollectionID());
		} else
			echo t('Unknown or deleted page');
	  ?>
      </td>      
     </tr>
     <tr class="ccm-list-record">
      <td class="result_dialog_label"><?php   echo t('Submitted by') ?></td>
      <td>
      <?php   
	  	$u = User::getByUserID($f->results->userID);
		if ($u instanceof User) { 
			echo '<a href="'.BASE_URL.DIR_REL.View::url('/dashboard/users/search?uID='.$u->getUserID()).'" target="_blank">'.$u->getUserName().'</a> ';
			echo t('(User ID: %s)', $u->getUserID());
		} else {
			if (!empty($fr->userID))
				echo t('Unknown or deleted user');
			else
				echo t('Guest');
		}
	  ?>
      </td>      
     </tr> 
     <tr class="ccm-list-record">
      <td class="result_dialog_label"><?php   echo t('Submitters IP') ?></td>
      <td><?php  echo $f->results->ip; ?></td>      
     </tr> 	 
	 <tr class="ccm-list-record">
      <td class="result_dialog_label"><?php   echo t('Used Browser') ?></td>
      <td><?php  echo (!empty($f->results->browser))?$f->results->browser:t('Unknown'); ?></td>      
     </tr> 
	 <tr class="ccm-list-record">
      <td class="result_dialog_label"><?php   echo t('Platform') ?></td>
      <td><?php  echo (!empty($f->results->platform))?$f->results->platform:t('Unknown'); ?></td>      
     </tr> 
	 <tr class="ccm-list-record">
      <td class="result_dialog_label"><?php   echo t('Screen resolution') ?></td>
      <td><?php  echo (!empty($f->results->resolution))?$f->results->resolution.t('px'):t('Unknown'); ?></td>      
     </tr>   
    </table>
	</fieldset>
	</div>
    </div>
    
	<div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix ccm-ui">
	 <div class="dialog-button">
	  <?php   echo $concrete_interface->button_js(t('Cancel'), 'ccm_blockWindowClose()', 'left'); ?>
	 </div>
	</div> 
	  

<script>
$(function() {
		
	$(".upload_preview").hover(
		function() {
			var thumb = $(this).find(".upload_preview-hover");
			if(thumb.length > 0) {
				var img = thumb.find("div");
				var pos = thumb.position();
				img.css("top",pos.top);
				img.css("left",pos.left);
				img.show();
			}
		},
		function() {
			var thumb = $(this).find(".upload_preview-hover");
			var img = thumb.find("div");
			img.hide();
		}
	);
	
	$('.element-body').parent('.ui-dialog-content').addClass('formidable-dialog-content');
	$('.ui-dialog-buttonpane').css({'width':$('.element-body').width()});	
});
</script>
