<?php    
defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('formidable/element', 'formidable');

class FormidableElementSelect extends FormidableElement {
	
	public $element_text = 'Selectbox';
	public $element_type = 'select';
	public $element_group = 'Basic Elements';	
	
	public $properties = array(
		'label' => true,
		'label_hide' => true,
		'required' => true,
		'options' => true,
		'option_other' => '',
		'min_max' => '',
		'tooltip' => true,
		'multiple' => true);
	
	public $dependency = array(
		'has_value_change' => true,
		'has_placeholder_change' => false
	);
	
	function __construct($elementID = 0) 
	{				
		parent::__construct($elementID);			

		$this->properties['min_max'] = array(
			'options' => t('Selected options')
		);
			
		$this->properties['option_other'] = array(
			'text' => t('Single text'),
			'textarea' => t('Textarea')
		);
	}
	
	public function generate()  
	{
		$form = Loader::helper('form');
								
		if ($this->multiple)
			$this->attributes['multiple'] = 'multiple';
			
		if (sizeof($this->attributes) > 0) 
			foreach ($this->attributes as $_name => $_value)
				$_attributes .= $_name.'="'.$_value.'" ';
														
		$_select = '<select name="'.$this->handle.'[]" id="'.$this->handle.'" '.$_attributes.'>';
		
		$_options = unserialize($this->options);	
		if (sizeof($_options) > 0) 
		{
			for ($i=0; $i<sizeof($_options); $i++)
			{			
				if (!$_options[$i]['value'])
					$_options[$i]['value'] = $_options[$i]['name'];
									
				$_selected = '';
				if (@in_array($_options[$i]['value'], (array)$this->value) || (empty($this->value) && $_options[$i]['selected'] === true))
					$_selected = 'selected="selected"';
					
				$_select .= '<option value="'.$_options[$i]['value'].'" '.$_selected.'>'.$_options[$i]['name'].'</option>';
			}
		}
				
		if (intval($this->option_other) != 0)
		{
			$selected = '';
			if (sizeof($this->value) > 0 && @in_array('option_other', $this->value))
				$selected = 'selected="selected"';
			
			$_select .= '<option value="option_other" '.$selected.'>'.$this->option_other_value.'</option>';
			
			$this->setAttribute('other', $form->{$this->option_other_type}($this->handle.'_other', $this->value_other, $this->attributes));
		}		
		$_select .= '</select>';
		
		$this->setAttribute('input', $_select);
	}
			
	public function validate() {
		
		Loader::model('formidable/validator', 'formidable');
		$validator = new FormidableValidator($this->elementID, $this->label, $this->dependency_validation);
		
		if ($this->required)
			$validator->required($this->request($this->handle));
			
		if ($this->min_max)	
			$validator->min_max($this->request($this->handle), $this->min_value, $this->max_value, $this->min_max_type);
		
		if ($this->option_other)
			$validator->option_other($this->request($this->handle), $this->request($this->handle.'_other'));
					
		return $validator->getList();
	}		
}