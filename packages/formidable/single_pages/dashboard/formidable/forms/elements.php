<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));
echo $concrete_dashboard->getDashboardPaneHeaderWrapper(t('Formidable Forms').' - '.t('Layout and elements'), t('List of rows and colums containing elements of this form'), false, false, array(Page::getByPath('/dashboard/formidable/forms/'), Page::getByPath('/dashboard/formidable/results/')));
?>
<div class="ccm-pane-body ccm-ui ccm-formidable">
	
    <?php   echo Loader::packageElement('dashboard/form/nav', 'formidable', array('f' => $f))?>
    
	<!--div class="ccm-dashboard-page-container">
		<div class="ccm-ui"></div>
	</div-->
	<form method="post" action="<?php   echo $this->action('save') ?>" id="ccm-form-record">
		<?php  echo $f->formID?$form->hidden('formID', $f->formID):''; ?>
		<div id="ccm-pane-body-left" style="display:none;">
			<?php   echo Loader::packageElement('dashboard/form/elements', 'formidable', array('elements' => $elements))?>
		</div>
		<fieldset>
			<div id="ccm-element-list">
				<div class="placeholder"><?php   echo t('Add row'); ?></div>
			</div>
		</fieldset>
		<div class="loader"></div>
		<div style="clear:both;"></div>
	</form>
</div>
<div class="ccm-pane-footer"></div>

<script>
var formID = <?php  echo $f->formID ?>;

$(function() {
	ccmFormidableLoadBackgroundImagesForLabels();
	
	$("#ccm-element-list").sortable({
		items: "div.element_row_wrapper",
		handle: ".mover",
		sort: function(event, ui) {
			$(this).removeClass( "ui-state-default" );
			
			ui.item.parents('.f-col').each(function() {
				var elnum = $('.element_row_wrapper:not(.element-empty)',this).length;
				if(elnum == 1) $('.element-empty', this).fadeIn();
				else $('.element-empty', this).hide();
			});
			
			$('.ui-sortable-placeholder').parents('.f-col').each(function() {
				var elnum = $('.element_row_wrapper:not(.element-empty)',this).length;
				if(elnum == 0) $('.element-empty', this).fadeIn();
				else $('.element-empty', this).hide();
			});
		},
		stop: function(event, ui) {
			var elemID = ui.item.attr('data-element_id');
			var newPos = ui.item.index();
			// Show or hide empty-elements
			$('.f-col').each(function() {
				var elnum = $('.element_row_wrapper:not(.element-empty)',this).length;
				if(elnum == 0) $('.element-empty', this).fadeIn();
				else $('.element-empty', this).hide();
			});
			
			var list = 'action=sort&formID='+formID;
			$("#ccm-element-list").find('.element_row_wrapper').each(function(i, row) {
				list += '&elements[]='+$(row).attr('data-element_id')+'&layout[]='+$(row).parent().parent().attr('data-id');
			});
			$.post(tools_url, list, function(r) {});
		}
	});
});
</script>
<?php 
echo $concrete_dashboard->getDashboardPaneFooterWrapper(false);
