<?php  
defined('C5_EXECUTE') or die(_("Access Denied."));
$this->inc('elements/header.php'); ?>
        
    <div id="wrapper">
    
    	<?php  $this->inc('elements/pagemeta.php'); ?>
        
        <div class="row">
            <div id="content" class="col_12">
                <?php  
				$a = new Area('Main');
				$a->display($c);
				?>
            </div><!-- #content ends -->
        </div><!-- .row ends -->

        <div class="row">
            <div class="col_8">
        		<div class="row">
           		 	<div class="col_12">
            		    <?php  
						$a = new Area('Column One');
						$a->display($c);
						?>
    	        	</div><!-- #col_12 ends -->
	        	</div><!-- .row ends -->
				
        		<div class="row">
           		 	<div class="col_6" style="width: 48.1%;
margin-right: 3.8%;">
           	    	 	<?php  
						$a = new Area('Column One 2');
						$a->display($c);
						?>
        	    	</div><!-- #col_6 ends -->
        		    	<div class="col_6 omega" style="width: 48.1%;">
           		 	    <?php  
						$a = new Area('Column Two 2');
						$a->display($c);
						?>
    	        	</div><!-- #col_6 ends -->
	        	</div><!-- .row ends -->
				
            </div><!-- #col_8 ends -->
            <div class="col_4 omega">
                <?php  
				$a = new Area('Column Two');
				$a->display($c);
				?>
            </div><!-- #col_4 ends -->
            
        </div><!-- .row ends -->
        
        <div class="row">
            <div class="col_12">
                <?php  
				$a = new Area('Main 2');
				$a->display($c);
				?>
            </div><!-- #content ends -->
        </div><!-- .row ends -->
        <?php  $this->inc('elements/vimeo_footer.php'); ?>
    </div><!-- #wrapper ends -->
    
<?php  $this->inc('elements/footer.php'); ?>