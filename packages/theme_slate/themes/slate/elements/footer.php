<?php   defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
    <footer>
    	<div class="row">
            <div class="col_3">
                <?php  
				$a = new GlobalArea('Footer');
				$a->display();
				?>
            </div><!-- .col_3 ends -->
            <div class="col_3">
                <?php  
				$a = new GlobalArea('Footer Column Two');
				$a->display();
				?>
            </div><!-- .col_3 ends -->
            <div class="col_3">
                <?php  
				$a = new GlobalArea('Footer Column Three');
				$a->display();
				?>
            </div><!-- .col_3 ends -->
            <div class="col_3 last">
                <?php  
				$a = new GlobalArea('Footer Column Four');
				$a->display();
				?>
            </div><!-- .col_3 ends -->
        </div><!-- .row ends -->
        <div class="row">
            <div id="credits" class="col_12 clearboth">
                <p class="left">&copy; <?php  echo date('Y')?> <?php  echo SITE?>. | 
                <a href="/Impressum">Impressum</a> | 
                <a href="/index.php/download_file/view/48/141/" target="_blank">AGB</a> | 
                <?php  
		$u = new User();
		if ($u->isRegistered()) { ?>
			<span class="sign-in"><?php  echo t('Currently logged in as <span class="user"><b>%s</b></span>.', $u->getUserName())?> <a href="<?php  echo $this->url('/login', 'logout')?>"><?php  echo t('Sign Out')?></a></span>
		<?php   } else { ?>
			<span class="sign-in"><a href="<?php  echo $this->url('/login')?>"><?php  echo t('Login')?></a>.</span>
	<?php   } ?></p>
            </div><!-- #credits ends -->
        </div><!-- .row ends -->
    </footer><!-- footer ends -->

</div><!-- #container ends -->

<?php   Loader::element('footer_required'); ?>

</body>
</html>