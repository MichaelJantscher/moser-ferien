1.5.2 - Fixed issue with Google Maps block zoom in/out functionality not working because of max-width set on images. Added check for Fancybox and Superfish so that other addons that include these libraries will work with theme.

1.5.1 - 10/03/2012 - Fixed issue with weird space in .row class fix from version 1.5 (in main.css file). See this thread: http://www.concrete5.org/marketplace/themes/slate/support/content-of-page-is-misaligned-leftwards-but-only-when-logged-in/#393639

1.5 - 10/02/2012 - Added display:inline to #logo p (to fix display issue in Chrome). Made floated images not float in mobile view. Added styling for input[type="tel"] in main.css file. Added max-width:100%; for iframes in main.css. Updated autonav templates (slate_main_nav.php and breadcrumb.php) to work with concrete5.6. Moved width:auto on img from main.css to ie.css (since it's only an IE issue). Fixed conflict with .row class in concrete5.6's ccm.app.css file (caused negative left margin on content when logged in). Updated pb_post.php for ProBlog. Made footer areas Global Areas.

1.4 - 02/08/2012 - Removed <meta  charset="utf-8" /> from header.php file. Added max-width:none to ie.css file to fix issue with text fields in contact form in IE. Added width:auto to img in main.css to fix issue with images being squished in IE8 in some cases. Updated logo area in header to work with Stacks instead of Scrapbooks (now to add your own logo, create a stack named Site Name and add your logo to that).

1.3.1 - 01/23/2012 - Fixed mistake made with CSS for slideshow block scaling (in 1.3 I added CSS for layouts fix instead of slideshow fix. removed that and added slideshow CSS fix back in).

1.3 - 01/18/2012 - Mobile view changes: Made main nav items full width and stacked vertically, added bottom margin to all columns, made copyright and credit lines in footer not float, added fix to make images in default concrete5 slideshow block scale like regular images,  made breadcrumbs not float to right.

1.2 - 12/02/2011 - Fixed bug with logo not displaying in IE8. Fixed z-index bug with main nav dropdowns in IE (dropdowns were behind some elements). Moved jQuery function for image hover opacity from superfish.js to functions.js.

1.1 - 11/26/2011 - Fixed issue with Breadcrumbs not showing all pages in trail. Removed page-meta div from blog_entry.php and pb_post.php and incorporated it in the pagemeta.php file. Updated ProBlog page type (pb_post.php) with newest trackback area. Added style for layouts in mobile.css file - made it so layout columns go to 100% width when viewed in mobile browser.

1.0 - 10/28/2011 - Initial Release