<?php $form = Loader::helper('form');
?>

<div class="ccm-ui">
		<table class="table table-striped table-bordered">
				<tr>
						<td>
								<?php echo t('Haustyp wählen') ?>
						</td>
						<td>
								<?php echo $form->text('house_type', $house_type) ?>
						</td>
				</tr>
		</table>
</div>