<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>

<?php if (strcmp($house_type, 'Wasserhaus') == 0): ?>
<div class="moser-table moser-table-wasserhaus">
		<?php else: ?>
		<div class="moser-table moser-table-sonnenhaus">
				<?php endif; ?>
				<table>
						<tbody>
						<tr>
								<td style="text-align: center;">
										<h3><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?php echo $house_type; ?> &nbsp; &nbsp;
														&nbsp; &nbsp; &nbsp;
														&nbsp; &nbsp;</strong></h3>
								</td>
								<td>Wochenpreis/ Person</td>
								<td>4 Tage (Sa-Mi)/ Person</td>
								<td>3 Tage (Mi-Sa)/ Person</td>
						</tr>
						<?php foreach ($entries as $entry): ?>
								<tr>
										<td style="text-align: left;"><?php echo $entry->getBlockDateString(); ?></td>
										<td><?php
												if(!is_numeric($entry->getWochenpreis()))
														echo $entry->getWochenpreis();
												else
														echo '€ ' . $entry->getWochenpreis() . ',-'; ?>
										</td>

										<td><?php
												if(!is_numeric($entry->getPreis_4tage()))
														echo $entry->getPreis_4tage();
												else
														echo '€ ' . $entry->getPreis_4tage() . ',-'; ?>
										</td>
										<td><?php
												if(!is_numeric($entry->getPreis_3tage()))
														echo $entry->getPreis_3tage();
												else
														echo '€ ' . $entry->getPreis_3tage() . ',-'; ?>
										</td>
								</tr>
						<?php endforeach; ?>
						</tbody>
				</table>
		</div>