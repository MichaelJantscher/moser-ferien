<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

class HauspreiseBlockController extends BlockController
{
		protected $btTable = 'btHauspreise';
		protected $btInterfaceWidth = "590";
		protected $btInterfaceHeight = "450";

		public $house_type = "";

		public function getBlockTypeName()
		{
				return t('Hauspreise');
		}

		public function getBlockTypeDescription()
		{
				return t('Auflistung der Preise des Sonnen- sowie Wasserhauses');
		}

		public function view()
		{
				$entries_db = $this->loadAllDBEntries();
				$this->set('entries', $entries_db);
				$this->set('house_type', $this->house_type);
		}

		public function edit()
		{
		}

		public function save($data)
		{
				parent::save($data);
		}

		private function loadAllDBEntries()
		{
				list($first_date, $last_date) = $this->calcTimeSpan();
				$db = Loader::db();
				$db_entries = $db->Execute("SELECT * FROM HausPreise WHERE haustyp='$this->house_type' AND datum_bis >= '$first_date' AND datum_von <= '$last_date'");

				Loader::model('hauspreiscollection');
				$hauspreis = new Hauspreiscollection($db_entries);
				return $hauspreis->getHauspreisArray();
		}

		private function calcTimeSpan()
		{
				$cur_date = date('d.m.Y');

				$cur_timestamp = strtotime($cur_date);
				$last_timestamp = strtotime('+1 years', $cur_timestamp);

				$first_date = date('Y-m-d', $cur_timestamp);
				$last_date = date('Y-m-d', $last_timestamp);
				return array($first_date, $last_date);
		}
}