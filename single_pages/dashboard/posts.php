<div class="ccm-ui">
		<div class="ccm-pane">
				<?php
				$dashboard = Loader::helper('concrete/dashboard');
				echo $dashboard->getDashboardPaneHeaderWrapper('Preise');
				?>
		</div>
		<div class="ccm-pane-body">
				<table class="table table-striped">
						<tr>
								<th>Haus</th>
								<th>Datum von</th>
								<th>Datum bis</th>
								<th>Wochenpreis</th>
								<th>4 Tage (Sa-Mi)</th>
								<th>3 Tage (Mi-Sa)</th>
								<th>Aktion</th>
						</tr>
						<?php foreach ($entries as $entry): ?>
								<tr>
										<td><?php echo $entry->getHaustyp(); ?></td>
										<td><?php echo $entry->getDatumVon(); ?></td>
										<td><?php echo $entry->getDatumBis(); ?></td>
										<td><?php
												if(strcmp($entry->getWochenpreis(), '-') == 0)
														echo $entry->getWochenpreis();
												else
														echo '€ ' . $entry->getWochenpreis(); ?>
										</td>

										<td><?php
												if(strcmp($entry->getPreis_4tage(), '-') == 0)
														echo $entry->getPreis_4tage();
												else
														echo '€ ' . $entry->getPreis_4tage(); ?>
										</td>
										<td><?php
												if(strcmp($entry->getPreis_3tage(), '-') == 0)
														echo $entry->getPreis_3tage();
												else
														echo '€ ' . $entry->getPreis_3tage(); ?>
										</td>
										<td>
												<a href="<?php echo $this->url('/dashboard/posts/add/edit', $entry->getID()); ?>" class="btn">Bearbeiten</a>
												<a href="<?php echo $this->url('/dashboard/posts/delete', $entry->getID()); ?>" class="btn btn-danger delete">Löschen</a>
										</td>
								</tr>
						<?php
						?>
						<?php endforeach; ?>
				</table>
				<a href="<?php echo $this->url('/dashboard/posts/add') ?>" class="btn btn-success">Hinzufügen</a>
		</div>
</div>