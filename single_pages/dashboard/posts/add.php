<?php $form = Loader::helper('form'); ?>
<?php $dth = Loader::helper('form/date_time'); ?>

<div class="ccm-ui">
		<div class="ccm-pane">
				<?php
				$dashboard = Loader::helper('concrete/dashboard');
				echo $dashboard->getDashboardPaneHeaderWrapper('Hinzufügen/ Editieren');
				?>
		</div>

		<form name="addForm" method="post" action="<?php echo $this->action('save') ?>" onsubmit="return validateForm()" method="POST">
				<div class="ccm-pane-body">
						<table class="table">
								<thead>
								<tr>
										<th>Informationen</th>
								</tr>
								</thead>

								<tbody>
								<tr>
										<td>
												Haus:
										</td>
										<td>
												<?php echo $form->text('haus', $post_typ); ?>
										</td>
								</tr>
								<tr>
										<td>
												Von:
										</td>
										<td>
												<?php if ($post_datum_von)
														echo $dth->date('datum_von', date('d-m-Y', strtotime($post_datum_von)));
												else
														echo $dth->date('datum_von', date('d-m-Y')); ?>
										</td>
								</tr>
								<tr>
										<td>
												Bis:
										</td>
										<td>
												<?php if ($post_datum_bis)
														echo $dth->date('datum_bis', date('d-m-Y', strtotime($post_datum_bis)));
												else
														echo $dth->date('datum_bis', date('d-m-Y')); ?>
										</td>
								</tr>
								<tr>
										<td>
												Wochenpreis:
										</td>
										<td>
												<?php echo $form->text('wochenpreis', $post_woche); ?>
										</td>
								</tr>
								<tr>
										<td>
												4 Tage (Sa-Mi):
										</td>
										<td>
												<?php echo $form->text('4tagepreis', $post_4tage); ?>
										</td>
								</tr>
								<tr>
										<td>
												3 Tage (Mi-Sa):
										</td>
										<td>
												<?php echo $form->text('3tagepreis', $post_3tage); ?>
										</td>
								</tr>
								</tbody>
						</table>
						<?php if ($post_id): ?>
								<?php echo $form->hidden('id_hidden', $post_id) ?>
						<?php endif; ?>
						<input type="submit" class="btn btn-primary" value="Speichern">
						<a href="<?php echo $this->url('/dashboard/posts') ?>" class="btn btn-danger">Cancel</a>
				</div>
		</form>
</div>