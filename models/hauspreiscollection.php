<?php
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('hauspreis');
Loader::helper('definevariable');

class Hauspreiscollection
{
		protected $hauspreis_array = array();
		protected $db_entries;

		public function __construct($db_entries)
		{
				$this->db_entries = $db_entries;
				$this->createModelObjects();
		}

		private function createModelObjects()
		{
				while ($obj = $this->db_entries->fetchRow())
				{
						array_push($this->hauspreis_array, new Hauspreis($obj));
				}
				usort($this->hauspreis_array, array($this, 'cmp'));
		}

		public function getHauspreisArray()
		{
				return $this->hauspreis_array;
		}

		private function cmp($a, $b)
		{
				if((strcmp($a->getHaustyp(), DefinevariableHelper::WASSERHAUS) == 0) && (strcmp($b->getHaustyp(), DefinevariableHelper::WASSERHAUS) == 0))
				{
						if($a->getTimestampDatumVon() < $b->getTimestampDatumVon())
								return -1;
						elseif ($a->getTimestampDatumVon() > $b->getTimestampDatumVon())
								return 1;
						else
								return 0;
				}
				elseif((strcmp($a->getHaustyp(), DefinevariableHelper::SONNENHAUS) == 0) && (strcmp($b->getHaustyp(), DefinevariableHelper::SONNENHAUS) == 0))
				{
						if($a->getTimestampDatumVon() < $b->getTimestampDatumVon())
								return -1;
						elseif ($a->getTimestampDatumVon() > $b->getTimestampDatumVon())
								return 1;
						else
								return 0;
				}
				elseif((strcmp($a->getHaustyp(), DefinevariableHelper::WASSERHAUS) == 0) && (strcmp($b->getHaustyp(), DefinevariableHelper::SONNENHAUS) == 0))
				{
						return -1;
				}
				elseif((strcmp($a->getHaustyp(), DefinevariableHelper::SONNENHAUS) == 0) && (strcmp($b->getHaustyp(), DefinevariableHelper::WASSERHAUS) == 0))
				{
						return 1;
				}
				else
						return 0;
		}
}