<?php
defined('C5_EXECUTE') or die("Access Denied.");

class Hauspreis
{
		protected $id;
		protected $haustyp = '';
		protected $wochenpreis = '';
		protected $preis_4tage = '';
		protected $preis_3tage = '';
		protected $datum_von = '';
		protected $datum_bis = '';

		public function __construct($row_entry = null)
		{
				if ($row_entry != null)
						$this->dbEntryToMembers($row_entry);
		}

		private function dbEntryToMembers($row_entry)
		{
				$this->id = $row_entry['id'];
				$this->haustyp = $row_entry['haustyp'];
				$this->wochenpreis = $row_entry['wochenpreis'];
				$this->preis_4tage = $row_entry['4tagepreis'];
				$this->preis_3tage = $row_entry['3tagepreis'];

				$datum_von_new = date("d.M.Y", strtotime($row_entry['datum_von']));
				$datum_bis_new = date("d.M.Y", strtotime($row_entry['datum_bis']));
				$this->datum_von = $datum_von_new;
				$this->datum_bis = $datum_bis_new;

		}

		public function getDBArrayOfMembers()
		{
				$member_array = array('haustyp' => $this->haustyp, 'wochenpreis' => $this->wochenpreis,
						'4tagepreis' => $this->preis_4tage, '3tagepreis' => $this->preis_3tage, 'datum_von' => $this->datum_von,
						'datum_bis' => $this->datum_bis);
				return $member_array;
		}

//		Getter Methods
		public function getID()
		{
				return $this->id;
		}

		public function getHaustyp()
		{
				return $this->haustyp;
		}

		public function getWochenpreis()
		{
				return $this->wochenpreis;
		}

		public function getPreis_4tage()
		{
				return $this->preis_4tage;
		}

		public function getPreis_3tage()
		{
				return $this->preis_3tage;
		}

		public function getDatumVon()
		{
				return $this->datum_von;
		}


		public function getDatumBis()
		{
				return $this->datum_bis;
		}

		public function getTimestampDatumVon()
		{
				$tmp = strtotime($this->datum_von);
				return $tmp;
		}

		public function getBlockDateString()
		{
				$stmp_from = strtotime($this->datum_von);
				$stmp_to = strtotime($this->datum_bis);
				$dt_from_year = date('Y', $stmp_from);
				$dt_to_year = date('Y', $stmp_to);
				$dt_from = date('d.M', $stmp_from);
				$dt_to = date('d.M', $stmp_to);
				$dt_from_ = $this->translateMonths($dt_from);
				$dt_to_ = $this->translateMonths($dt_to);

				if ($dt_from_year != $dt_to_year) {
						return $dt_from_ . ' - ' . $dt_to_ . '. ' . $dt_to_year;
				} else {
						return $dt_from_ . ' - ' . $dt_to_;
				}
		}

		private function translateMonths($date_string)
		{
				if (strpos($date_string, 'Mar'))
						$tmp = str_replace('Mar', 'Mär', $date_string);
				elseif (strpos($date_string, 'May'))
						$tmp = str_replace('May', 'Mai', $date_string);
				elseif (strpos($date_string, 'Oct'))
						$tmp = str_replace('Oct', 'Okt', $date_string);
				elseif (strpos($date_string, 'Dec'))
						$tmp = str_replace('Dec', 'Dez', $date_string);
				else
						return $date_string;

				return $tmp;
		}


//		Setter Methods
		public
		function setHaustyp($typ)
		{
				$this->haustyp = $typ;
		}

		public
		function setWochenpreis($preis)
		{
				$this->wochenpreis = $preis;
		}

		public
		function setPreis_4tage($preis)
		{
				$this->preis_4tage = $preis;
		}

		public
		function setPreis_3tage($preis)
		{
				$this->preis_3tage = $preis;
		}

		public
		function setDatumVon($datum_von)
		{
				$this->datum_von = $datum_von;
		}

		public
		function setDatumBis($datum_bis)
		{
				$this->datum_bis = $datum_bis;
		}

}