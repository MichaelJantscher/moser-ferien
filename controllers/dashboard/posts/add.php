<?php
defined('C5_EXECUTE') or die("Acces Denied.");

class DashboardPostsAddController extends Controller
{
		public function save()
		{
				Loader::model('hauspreis');
				$hauspreis = new Hauspreis();

				$haustyp = $this->post('haus');
				$wochenpreis = $this->post('wochenpreis');
				$tage_4 = $this->post('4tagepreis');
				$tage_3 = $this->post('3tagepreis');
				$datum_von = $this->post('datum_von');
				$datum_bis = $this->post('datum_bis');

				$hauspreis->setHaustyp($haustyp);
				$hauspreis->setWochenpreis($wochenpreis);
				$hauspreis->setPreis_4tage($tage_4);
				$hauspreis->setPreis_3tage($tage_3);
				$hauspreis->setDatumVon($datum_von);
				$hauspreis->setDatumBis($datum_bis);

				$db = Loader::db();

				if ($this->post('id_hidden'))
				{
						$update_id = $this->post('id_hidden');

						$datum_von_new = date("Y-m-d", strtotime($datum_von));
						$datum_bis_new = date("Y-m-d", strtotime($datum_bis));

						$db->Execute("UPDATE HausPreise SET haustyp='$haustyp', wochenpreis='$wochenpreis',4tagepreis='$tage_4', 3tagepreis='$tage_3', 
													datum_von='$datum_von_new', datum_bis='$datum_bis_new' WHERE id='$update_id'");
				}
				else
				{
						$current_increment_id_row = $db->Execute("SELECT MAX(id) FROM HausPreise");
						$current_increment_id_row_ = $current_increment_id_row->fetchRow();
						$current_increment_id = $current_increment_id_row_['MAX(id)'];
						$next_increment_id = $current_increment_id + 1;

						$datum_von_new = date("Y-m-d", strtotime($datum_von));
						$datum_bis_new = date("Y-m-d", strtotime($datum_bis));

						$db->Execute("INSERT INTO HausPreise (id, haustyp, wochenpreis, 4tagepreis, 3tagepreis, datum_von, datum_bis)
													VALUES ($next_increment_id, '$haustyp', '$wochenpreis', '$tage_4', '$tage_3', '$datum_von_new', '$datum_bis_new')");
				}
				$this->redirect('dashboard/posts');
		}

		public function edit($id_)
		{
				$db = Loader::db();
				$db_entries = $db->Execute("SELECT * FROM HausPreise WHERE id='$id_'");
				Loader::model('hauspreiscollection');
				$hauspreis = new Hauspreiscollection($db_entries);
				$hauspreis_array = $hauspreis->getHauspreisArray();
				$this->set('post_id', $hauspreis_array[0]->getID());
				$this->set('post_typ', $hauspreis_array[0]->getHaustyp());
				$this->set('post_woche', $hauspreis_array[0]->getWochenpreis());
				$this->set('post_4tage', $hauspreis_array[0]->getPreis_4tage());
				$this->set('post_3tage', $hauspreis_array[0]->getPreis_3tage());
				$this->set('post_datum_von', $hauspreis_array[0]->getDatumVon());
				$this->set('post_datum_bis', $hauspreis_array[0]->getDatumBis());
		}

		public function view()
		{
				$val = Loader::helper('validation/form');
				$val->setData($this->post());
				$val->addRequired('haus', 'Bitte geben Sie einen Haustyp [Wasserhaus oder Sonnenhaus] an.');
				if($val->test())
				{
						// proceed...
				}
				else
				{
						$error_array = $val->getError()->getList();
						$this->set('errorArray', $error_array);
				}
		}

		public function on_before_render()
		{
				$html = Loader::helper('html');
				$this->addHeaderItem($html->javascript('list.js'));
		}
}