<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardPostsController extends Controller
{
		private function loadAllDBEntries()
		{
				$db = Loader::db();
				$db_entries = $db->Execute("SELECT * FROM HausPreise");

				Loader::model('hauspreiscollection');
				$hauspreis = new Hauspreiscollection($db_entries);
				return $hauspreis->getHauspreisArray();
		}

		public function view()
		{
				$entries_db = $this->loadAllDBEntries();
				$this->set('entries', $entries_db);
		}

		public function delete($id)
		{
				$db = Loader::db();
				$db->Execute("DELETE FROM HausPreise WHERE id='$id'");
				$this->redirect('/dashboard/posts');
		}

		public function on_before_render()
		{
				$html = Loader::helper('html');
				$this->addHeaderItem($html->javascript('list.js'));
		}
}